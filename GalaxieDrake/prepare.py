#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: "Tuux" <tuxa@rtnp.org> all rights reserved

import os
import glob
import fnmatch
import sys
import subprocess
import argparse
from utils import Utils
from taskspooler import TaskSpooler
from subripper import SubRipper


class Prepare(object):
    """
    Prepare will search recursively video file's and stack transcode job for they inside a task-spooler.

    It have capability to be Interactive, or AutoYes/AutoNo.
    During the scan you have capability to display a progress bar.

    If a job is all ready in queue, it will not be added again.
    Task-spooler will start job in detached mode , then you can logout and back later.

    Yet Prepare is the entrance point for GalaxieDrake, where GalaxieDrake is semi automatized Text Based
    Media Center ToolKit.
    """
    def __init__(self, directory=None, enable_progress=False, yes=False, no=False, debug=0):
        """
        Prepare function

        :param directory: a valid path
        :param enable_progress: if the progress bar should be display
        :type enable_progress: bool
        :param yes: enable auto yes to the question
        :type yes: bool
        :param no: enable auto no to the question
        :type no: bool
        :param debug: debug level for verbose output
        :type debug: int
        """
        self.utils = Utils()
        self.taskspooler = TaskSpooler()
        self.subripper = SubRipper()

        self.name = str(os.path.basename(os.path.splitext(sys.argv[0])[0]).title())
        self.version = 0.3
        self.pattern_exception = '* - *p.mkv'

        self.files_to_transcode = list()

        # Internal
        self._query_label = ''
        self.yes = yes
        self.no = no
        self.enable_progress = enable_progress

        self.working_directory = os.path.realpath(directory)

        self.utils.check_transcoder_requirement()

        # Everything is test we can start to scan directory
        self.display_resume_before_scan()

        # make the scan of the working directory
        self.scan()

        # summary of file(s) to encode:
        self.display_resume_after_scan()

        # Look if "no" and if file(s) to transcode have been found
        if not self.no and len(self.get_files_to_transcode()):
            # Look if "yes" or Ask the yes-no question
            if self.yes or self.ask_if_ok_to_transcode():
                # Create one task by file to transcode and send it to taskspooler queue
                self.create_tasks()
            else:
                # Here the human user have respond "no" to the yes-no question
                self.display_nothing_do_do()
        else:
            # No file to transcode have been found or --no have been use
            self.display_nothing_do_do()

    def create_tasks(self):
        self.display_number_of_files_to_transcode()
        # Add task(s)
        count = 1

        for src_file in self.get_files_to_transcode():
            job_subripper_id = None
            sys.stdout.write("Task: " + str(count) + ", " + src_file + "\n")
            sys.stdout.flush()
            if not self.check_if_subripper_job_exist(src_file):
                job_subripper_id = self.add_subripper_job(src_file)
            if not self.check_if_transcode_job_exist(src_file):
                if job_subripper_id:
                    self.add_transcoder_job(src_file=src_file, check_job_id=job_subripper_id)
                else:
                    self.add_transcoder_job(src_file=src_file)
            count += 1

    def display_number_of_files_to_transcode(self):
        # Summary about number of files to transcode
        if len(self.get_files_to_transcode()) == 1:
            sys.stdout.write('Creation of 1 task:\n')
        else:
            sys.stdout.write('Creation of ' + str(len(self.get_files_to_transcode())) + ' tasks:\n')

    @staticmethod
    def display_nothing_do_do():
        sys.stdout.write('Nothing to do ...\n')
        sys.stdout.flush()

    def display_resume_after_scan(self):
        if len(self.get_files_to_transcode()):
            if len(self.get_files_to_transcode()) == 1:
                sys.stdout.write('1 File found:\n')
            else:
                sys.stdout.write(str(len(self.get_files_to_transcode())) + ' Files founds:\n')
            for file_to_transcode in self.get_files_to_transcode():
                sys.stdout.write(' ' + file_to_transcode + '\n')

    def display_add_job_summary(self, job_id=None, command=None):
        """
        Display a

        :param job_id: The job id as return by TaskSpooler.add_transcoder_job() or  TaskSpooler.add_subripper_job()
        :type job_id: int
        :param command: command with they argument's in list format
        :type command: list
        :raise TypeError: When job_id is not a int type
        :raise TypeError: When command is not a list type
        """
        # Trey to exist as soon of possible
        if type(job_id) != int:
            raise TypeError('"job_id" must be a int type')
        if type(command) != list:
            raise TypeError('"command" must be a list type')
        # Everything look ok we can continue
        sys.stdout.write(' ID: ' + str(job_id) + ', State: ' + self.taskspooler.get_job_state(job_id) + '\n')
        if command:
            cmd_to_display = ''
            for element in command:
                cmd_to_display += element
                cmd_to_display += ' '

            # Finally we have something to display
            try:
                sys.stdout.write(' ' + str(cmd_to_display.rstrip()) + '\n')
            except UnicodeEncodeError:
                sys.stdout.write(' ' + cmd_to_display.rstrip() + '\n')
            sys.stdout.flush()

    def display_resume_before_scan(self):
        """ Display a summary before the scan"""
        if not self.utils.get_nice_path():
            sys.stdout.write('Warning: Nice is require, for set low process priority\n')
            sys.stdout.write('Warning: ' + self.get_name() + ' will continue without\n')
            sys.stdout.write(self.get_prepare_version())
            sys.stdout.write(' / ')
            sys.stdout.write(self.subripper.get_name() + ' v' + str(self.subripper.get_version()))
            sys.stdout.write(' / ')
            sys.stdout.write(self.taskspooler.get_version())
            sys.stdout.write('\n')
            sys.stdout.flush()
        else:
            sys.stdout.write(self.get_prepare_version())
            sys.stdout.write(' / ')
            sys.stdout.write(self.subripper.get_name() + ' v' + str(self.subripper.get_version()))
            sys.stdout.write(' / ')
            sys.stdout.write(self.taskspooler.get_version())
            sys.stdout.write(' / ')
            sys.stdout.write(self.utils.get_nice_version())
            sys.stdout.write('\n')
            sys.stdout.flush()

        sys.stdout.write(' Transcoder Path: ' + self.utils.get_transcoder_path())
        sys.stdout.write('\n')
        sys.stdout.write(' Source Directory: ' + self.working_directory + '/')
        sys.stdout.write('\n')
        sys.stdout.write('Searching for: ')
        sys.stdout.write('\n')
        for file_pattern in self.get_extensions_list():
            sys.stdout.write(' ' + file_pattern.upper()[2:])
        sys.stdout.write('\n')
        sys.stdout.write(' Exception Pattern: \"' + self.get_explude_pattern() + '\"\n')
        sys.stdout.flush()

    def scan(self):
        # Counter for the Text Progress Bar
        count = 1
        # For each file patterns extension
        for file_pattern in self.get_extensions_list():
            if self.enable_progress:
                # Call Text Progress Bar
                rows, columns = os.popen('stty size', 'r').read().split()
                self.utils.cli_progress_bar(
                    'Scanning for ' + file_pattern.upper()[2:] + ': ',
                    int(round(100 * count / len(self.get_extensions_list()))),
                    100,
                    int(columns)
                )

            # Scan directory recursively for Lower and Upper case file extension
            files_lower = self._get_files_to_scan(self.working_directory, file_pattern.lower())
            files_upper = self._get_files_to_scan(self.working_directory, file_pattern.upper())

            # Add Lower case file extension to the final file list to encode
            if len(files_lower):
                for file_lower in files_lower:
                    if not fnmatch.fnmatch(file_lower, self.get_explude_pattern()):
                        self.get_files_to_transcode().append(file_lower)
            # Add Upper case file extension to the final file list to encode
            if len(files_upper):
                for file_upper in files_upper:
                    if not fnmatch.fnmatch(file_upper, self.get_explude_pattern()):
                        self.get_files_to_transcode().append(file_upper)

            # Counter for the Text Progress Bar
            count += 1

        # Clear the Text Progress Bar line
        sys.stdout.write("\x1b[2K")
        sys.stdout.write("\r")
        sys.stdout.flush()

    def get_name(self):
        """
        Get the script name

        :return: script name
        :rtype: str
        """
        return self.name

    def get_version(self):
        """
        Get script version

        :return: script version
        :rtype: float
        """
        return self.version

    def get_prepare_version(self):
        """
        Get script name and version

        :return: script information's
        :rtype: str
        """
        return str(self.get_name() + ' v' + str(self.get_version()))

    @staticmethod
    def get_extensions_list():
        """
        Return list of extensions file the script will search during the scan

        :return: the extensions list
        :rtype: list
        """
        # Limit the search
        # extension_list = ['*.ts']

        return [
            '*.ts',
            '*.mkv',
            '*.3g2',
            '*.3gp',
            '*.asf',
            '*.asx',
            '*.avi',
            '*.flv',
            '*.m4v',
            '*.mov',
            '*.mp4',
            '*.mpg',
            '*.rm',
            '*.swf',
            '*.vob',
            '*.wmv'
        ]

    def get_explude_pattern(self):
        """
        In case of multi-resolution file like: "filename - 1080p.mkv"

        All multi-resolution files will be ignore during scan , that because they file are not consider as
        transcoding source.

        :return: a exclusion pattern
        :rtype: str
        """
        return self.pattern_exception

    def set_exclude_pattern(self, pattern='* - *p.mkv'):
        """
        Set the pattern to exclude during scan, all files it match with it pattern will be ignores.

        :param pattern: a regexpr pattern
        :type pattern: str
        """
        if type(pattern) != str:
            raise TypeError('"pattern" must be a str type')
        if self.get_explude_pattern() != pattern:
            self.pattern_exception = pattern

    def ask_if_ok_to_transcode(self, question=None, default="yes"):
        """
        Ask a yes/no question via raw_input() and return their answer.
    
        "question" is a string that is presented to the user.
        "default" is the presumed answer if the user just hits <Enter>.
        It must be "yes" (the default), "no" or None (meaning an answer is required of the user).

        :return: True for "yes" or False for "no"
        :rtype: True or False
        """
        if len(self.get_files_to_transcode()):
            if len(self.get_files_to_transcode()) == 1:
                self._set_query_label('Do you want creat transcoding task for it file ?')
            else:
                self._set_query_label('Do you want creat transcoding tasks for they ' + str(
                    len(self.get_files_to_transcode())) + ' files ?')

        valid = {
            "yes": True,
            "y": True,
            "ye": True,
            "no": False,
            "n": False
        }

        if default is None:
            prompt = " [y/n] "
        elif default == "yes":
            prompt = " [Y/n] "
        elif default == "no":
            prompt = " [y/N] "
        else:
            raise ValueError("invalid default answer: '%s'" % default)

        sys.stdout.write('\n')
        while True:
            sys.stdout.write(self._get_query_label() + prompt)
            sys.stdout.flush()
            choice = raw_input().lower()
            if default is not None and choice == '':
                return valid[default]
            elif choice in valid:
                return valid[choice]
            else:
                sys.stdout.write("Please respond with 'yes' or 'no' (or 'y' or 'n').\n")
                sys.stdout.flush()

    def add_subripper_job(self, src_file):
        job_id = self.taskspooler.add_job(
            command=self.get_subripper_job_cmd(src_file)
        )
        if job_id:
            self.display_add_job_summary(
                job_id=job_id,
                command=self.get_subripper_job_cmd(src_file)
            )
            return job_id
        return None

    def get_subripper_job_cmd(self, src_file):
        """
        Return the Add Job subripper command line, by looking different thing like path or nice capability

        :param src_file: A Absolute path of a file where execute SubRipper
        :type src_file: str
        :return: The command line with they argument's store inside a list
        :rtype: list
        :raise TypeError: When src_file is not a str
        """
        # Try to exist as soon of possible
        if type(src_file) != str:
            raise TypeError('"src_file" must be a str')

        cmd = list()
        if self.utils.get_nice_path():
            cmd.append(unicode(self.utils.get_nice_path(), 'utf-8'))
            cmd.append(unicode("-n", 'utf-8'))
            cmd.append(unicode(str(self.utils.get_nice_priority()), 'utf-8'))
        cmd.append(unicode(self.utils.get_subripper_path(), 'utf-8'))
        cmd.append(unicode(src_file, 'utf-8'))

        # Finally return something
        return cmd

    def check_if_subripper_job_exist(self, src_file):
        """
        Check if the TaskSpooler server have all ready a Transcode job, by searching inside jobs list.

        :param src_file: full path of file to execute subripper
        :type src_file: str
        :return: True if the job exist , False if job is not found
        :rtype: bool
        """
        searching_command_txt = ""
        if self.utils.get_nice_path():
            searching_command_txt = searching_command_txt + str(self.utils.get_nice_path())
            searching_command_txt = searching_command_txt + " "
            searching_command_txt = searching_command_txt + str("-n")
            searching_command_txt = searching_command_txt + " "
            searching_command_txt = searching_command_txt + str(self.utils.get_nice_priority())
            searching_command_txt = searching_command_txt + " "
        searching_command_txt = searching_command_txt + str(self.utils.get_subripper_path())
        searching_command_txt = searching_command_txt + " "
        searching_command_txt = searching_command_txt + str(src_file)

        # Deal with job_list
        if self.taskspooler.get_jobs_list():
            for element in self.taskspooler.get_jobs_list():
                if searching_command_txt in element['Command']:
                    sys.stdout.write(" SubTitles task all ready in queue with ID: " + element['ID'])
                    sys.stdout.write('\n')
                    sys.stdout.flush()
                    return True
        return False

    def add_transcoder_job(self, src_file=None, check_job_id=None):
        """
        Add a Transcoder job to the TaskSpooler Server

        :param src_file: A absolute path of the source file where execute the Transcoder
        :type src_file: str
        :param check_job_id: A job ID where the exist code will be check before execute the new Transcoder job.
        :type check_job_id: int
        :return: a job id as return by the TaskSpooler server or None if can't
        :rtype: int or None
        :raise TypeError: When src_file is not a str
        :raise TypeError; When check_job_id is not a int
        """
        # Try to exist as soon of possible
        if type(src_file) != str:
            raise TypeError('"src_file" is not a str type')
        if type(check_job_id) != int and check_job_id is not None:
            raise TypeError('"check_job_id" is not a int type or None')

        # Everything look OK we can continue
        if check_job_id:
            job_id = self.taskspooler.add_job(
                command=self.get_transcoder_job_cmd(src_file),
                check_job_id=check_job_id
            )
        else:
            job_id = self.taskspooler.add_job(
                command=self.get_transcoder_job_cmd(src_file)
            )

        if job_id:
            self.display_add_job_summary(
                job_id=job_id,
                command=self.get_transcoder_job_cmd(src_file)
            )
            return job_id
        else:
            return None

    def get_transcoder_job_cmd(self, src_file):
        """
        Return the Transcoder command line and they argument's store inside a list.

        :param src_file: a absolute path of a source file where execute SubRipper
        :type src_file: str
        :return: The command follow by argument's inside a list
        :rtype: list
        """
        cmd = list()
        if self.utils.get_nice_path():
            cmd.append(unicode(self.utils.get_nice_path(), 'utf-8'))
            cmd.append(unicode("-n", 'utf-8'))
            cmd.append(unicode(str(self.utils.get_nice_priority()), 'utf-8'))
        cmd.append(unicode(self.utils.get_transcoder_path(), 'utf-8'))
        cmd.append(unicode(src_file, 'utf-8'))
        # Finally return something
        return cmd

    def check_if_transcode_job_exist(self, src_file):
        """
        Check if the TaskSpooler server have all ready a Transcode job, by searching inside jobs list.

        :param src_file: full path of file to transcode
        :type src_file: str
        :return: True if the job exist , False if job is not found
        :rtype: bool
        """
        searching_command_txt = ""
        if self.utils.get_nice_path():
            searching_command_txt = searching_command_txt + str(self.utils.get_nice_path())
            searching_command_txt = searching_command_txt + " "
            searching_command_txt = searching_command_txt + str("-n")
            searching_command_txt = searching_command_txt + " "
            searching_command_txt = searching_command_txt + str(self.utils.get_nice_priority())
            searching_command_txt = searching_command_txt + " "
        searching_command_txt = searching_command_txt + str(self.utils.get_transcoder_path())
        searching_command_txt = searching_command_txt + " "
        searching_command_txt = searching_command_txt + str(src_file)

        # Deal with job_list
        if self.taskspooler.get_jobs_list():
            for element in self.taskspooler.get_jobs_list():
                if searching_command_txt in element['Command']:
                    sys.stdout.write(" Transcoding task all ready in queue with ID: " + element['ID'])
                    sys.stdout.write('\n')
                    sys.stdout.flush()
                    return True
        return False

    def set_files_to_transcode(self, files_to_transcode=None):
        """
        Set files to transcode as a list, where each entry is the absolute os path of a file to transcode.

        :param files_to_transcode: a list of files to transcode
        :type files_to_transcode: list
        :raise TypeError: when files_to_transcode is not a list
        """
        if files_to_transcode is None:
            files_to_transcode = list()

        if type(files_to_transcode) != list:
            raise TypeError('"files_to_transcode" must be a list type')

        if self.get_files_to_transcode() != files_to_transcode:
            self.files_to_transcode = files_to_transcode

    def get_files_to_transcode(self):
        """
        Get the files to transcode list

        :return: files to transcode
        :rtype: list
        """
        return self.files_to_transcode

    @staticmethod
    def _get_directory_to_scan(base):
        return [x for x in glob.iglob(os.path.join(base, '*')) if os.path.isdir(x)]

    def _get_files_to_scan(self, base, pattern):
        list_to_return = []
        list_to_return.extend(glob.glob(os.path.join(base, pattern)))
        dirs = self._get_directory_to_scan(base)
        if len(dirs):
            for d in dirs:
                list_to_return.extend(self._get_files_to_scan(os.path.join(base, d), pattern))
        return list_to_return

    def _get_query_label(self):
        """
        Get the question label question

        :return: query label
        :rtype: str
        """
        return self._query_label

    def _set_query_label(self, query_label=None):
        """
        Set the text for the confirmation question

        :param query_label: the label text to set
        :type query_label: str
        :raise TypeError: when query_label is not a str type
        """
        if type(query_label) != str:
            raise TypeError('"query_label" must be a str type')
        if self._get_query_label() != query_label:
            self._query_label = query_label

if __name__ == "__main__":

    def check_directory(directory):
        if os.path.isfile(directory):
            raise argparse.ArgumentTypeError(
                '"{0}" is not a directory\n'
                'You should create a subdirectory '
                'for transcode {1}...'.format(directory, os.path.basename(directory))
            )
        if not os.path.isdir(directory):
            raise argparse.ArgumentTypeError(
                '"{0}" does not exist\n'
                'Maybe wrong path...'.format(directory)
            )
        elif not os.access(directory, os.R_OK):
            raise argparse.ArgumentTypeError(
                'Directory "{0}" can\'t be read\n'
                'Maybe missing permissions...'.format(directory)
            )
        return directory

    debug_level = 0
    auto_yes = False
    auto_no = False
    enable_progress = False
    parser = argparse.ArgumentParser(
        description="GalaxieDrakeCLI prepare script",
        prog='prepare.py',
        usage='%(prog)s [options] directory\n'
              'Please, invoke it script with the path of a directory\n'
              'It directory will be use as Movie source directory\n'
              'Few Examples:\n'
              ' %(prog)s ./\n'
              ' %(prog)s ../a/other/directory\n'
              ' %(prog)s /full/path/to/the/directory\n'
              ' %(prog)s \"/full/path/to/the/directory/With space name/\"\n'
    )
    parser.add_argument('-y', '--yes', '--assume-yes',
                        action='store_true',
                        help='Automatic yes to prompts; '
                             'assume "yes" as answer to all prompts and run non-interactively.'
                        )
    parser.add_argument('-n', '--no', '--assume-no',
                        action='store_true',
                        help='Automatic "no" to all prompts; '
                             'assume "no" as answer to all prompts and run non-interactively.'
                        )
    parser.add_argument('-P', '--progress',
                        action='store_true',
                        help='This option tells %(prog)s to print information showing the progress of the scan. '
                             'This gives a bored user something to watch.'
                        )
    parser.add_argument('-v', '--verbosity',
                        action="count",
                        default=0,
                        help="increase output verbosity, -v or -vv or -vvv are accepted"
                        )
    parser.add_argument('-V', '--version',
                        action='version',
                        version='%(prog)s v0.2 - GalaxieDrakeCLI prepare script'
                        )
    parser.add_argument('directory',
                        type=check_directory,
                        action="store",
                        help='Working directory where start the media scan',
                        metavar='directory'
                        )

    args = parser.parse_args()

    if args.verbosity:
        debug_level = args.verbosity

    if args.yes:
        auto_yes = args.yes

    if args.no:
        auto_no = args.no

    if args.progress:
        enable_progress = args.progress

    if args.directory:
        p = Prepare(
            directory=args.directory,
            yes=auto_yes,
            no=auto_no,
            enable_progress=enable_progress,
            debug=debug_level
        )
