#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: "Tuux" <tuxa@rtnp.org> all rights reserved

import os
import sys
import subprocess
import re
import argparse
import pycountry
import fcntl
import select
from utils import Utils


class SubRipper(object):
    """
    Yes that what everyone want or need a Ripper of MKV SubTitles.

    That tool is here for help everyone during him numerical life, without special attention of commercial thing.
    You certainly have to respect the law the better you can , the necessary time to permit law to match with a
    natural numerical life.

    Why making a strange thing like that ?

    That because the Kodi Naming convention will certainly a good candidate to be the future storage format of
    SubTitles. A simple SubTitles extractor is necessary in that case.
    I can't say what will be the future format, but during the transition period SubRipper will be usefull.

    You should not RIP commercial thing, it's to mush honor for them, you have to consider it seriously and
    use it tool at you own risk. I'm not and i'll not be responsible about the usage you making of that tool.
    """

    def __init__(self, filename=None, app_name=None, app_version=None, enable_progress=False, verbose=0, utils=None):

        self.input_file = None
        self.working_directory = None
        if filename is None:
            self.set_mkv_file_path()
        else:
            self.set_mkv_file_path(filename)

        self.name = None
        if app_name is None:
            self.set_name()
        else:
            self.set_name(app_name)

        self.version = None
        if app_version is None:
            self.set_version()
        else:
            self.set_version(app_version)

        self.utils = None
        if utils is None:
            self.utils = Utils()
        else:
            self.utils = utils

        self.debug = verbose
        self.enable_progress_bar = enable_progress
        self.scanned_subtitles_list = list()
        self.subtitles_it_have_default = list()
        self.subtitles_it_have_forced = list()
        self.scan_subtitles_detect_line = "--Output=Text;%ID%,%Language%,%Default%,%Forced%," \
                                          "%Format%,%CodecID%,%Codec/Info%:::"

    def run(self):
        """
        Make the work

        """
        self._display_header()
        self._display_summary_generate_mks_file()
        self.generate_mks_file(file_source=self.get_mkv_file_path(), file_destination=self.get_mks_file_path())
        self.scan(filename=self.get_mks_file_path())
        if self.get_scan():
            # subtitles have been found
            self._display_summary_after_scan()
            self.extract_subtitles(file_source=self.get_mks_file_path())
            self.check_forced_flag()
            self.rename_subtitles_files()
            os.remove(self.get_mks_file_path())
            self.set_mkv_file_path()
        else:
            self._display_nothing_do_do()

    def generate_mks_file(self, file_source=None, file_destination=None):
        """
        Generate a MKS file suppose to be a MKV container with only subtitles
        """
        if file_source is None:
            if self.get_mkv_file_path() is not None:
                file_source = self.get_mkv_file_path()
            else:
                raise SystemError('Impossible to generate a MKS file without source file')
        else:
            if not os.path.isfile(os.path.abspath(file_source)):
                raise SystemError('Source file: "' + str(file_source) + '" don\'t exist')

        if file_destination is None:
            if self.get_mks_file_path() is not None:
                file_destination = self.get_mks_file_path()
            else:
                raise SystemError('Impossible to generate a MKS file without a destination file')

        # start subprocess
        proc = subprocess.Popen(
            self.get_cmd_for_generate_a_mks_file(file_source=file_source, file_destination=file_destination),
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )

        # set non-blocking mode
        outfile = proc.stdout
        outfd = outfile.fileno()
        file_flags = fcntl.fcntl(outfd, fcntl.F_GETFL)
        fcntl.fcntl(outfd, fcntl.F_SETFL, file_flags | os.O_NDELAY)

        # use 'select' for reading
        while True:
            ready = select.select([outfd], [], [])  # wait for input
            if outfd in ready[0]:
                outchunk = outfile.read()
                if not outchunk:
                    break
                else:
                    if self.enable_progress_bar:
                        # remove \r and \n is important for the rest of math
                        outchunk = re.sub('\r', '', outchunk)
                        outchunk = re.sub('\n', '', outchunk)
                        if re.search('^[a-zA-Z0-9]+\s:\s\d+|\d%$', outchunk):
                            percentage = re.findall('(\d+|\d)', outchunk)
                            rows, columns = os.popen('stty size', 'r').read().split()

                            self.utils.cli_progress_bar(
                                'Generating: ',
                                int(percentage[0]),
                                100,
                                int(columns)
                            )
        # Clear the Text Progress Bar line
        if self.enable_progress_bar:
            sys.stdout.write("\x1b[2K")
            sys.stdout.write("\r")
            sys.stdout.flush()

    def scan(self, filename=None):
        """
        Use MediaInfo for make a scan of subtitles and set scan

        If 'file_to_scan' is None the scanned file will be the one return by SubRipper.get_file()

        :param filename: a file to scan
        :type filename: str or None
        :return: False if no subtitles have been add to subtitles list
        :rtype: bool
        :raise SystemError: when mediainfo can't be found in environement $PATH variable
        """
        if filename is None:
            if self.utils.check_if_file_exist(self.get_mks_file_path()):
                file_to_scan = self.get_mks_file_path()
            else:
                return False
        else:
            if self.utils.check_if_file_exist(filename):
                file_to_scan = filename
            else:
                return False

        proc = subprocess.Popen(
            self.get_cmd_for_scan_subtitles(file_to_scan),
            shell=False,
            bufsize=1,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT
        )

        # self.set_scan(list())

        # create a temporary list for can compare, with actual scan_list
        new_scan_list = list()

        # read the stdout without last \n
        line = proc.stdout.readline()[:-1]

        # everything is export in one line with ::: separator
        subtitles = re.split(r':::', line)

        # for each detected subtitles
        for subtitles_info in subtitles:
            # get information's , where , is the separator
            info = re.split(r',', subtitles_info)

            # we have 7 information's to get
            if len(info) == 7:
                child_info = {
                    'ID': int(info[0]) - 1,
                    'Language': info[1],
                    'Default': info[2],
                    'Forced': info[3],
                    'Format': info[4],
                    'CodecID': info[5],
                    'CodecInfo': info[6],
                    'RandomID': self.utils.new_id()
                }

                try:
                    lang = pycountry.languages.lookup(child_info['Language'])
                    child_info['Language'] = lang.name
                except LookupError:
                    child_info['Language'] = 'Unknown'

                new_scan_list.append(child_info)

        # we have have detect one or more subtitles
        if len(new_scan_list):
            if self.get_scan() != new_scan_list:
                self.set_scan(subtitles_list=new_scan_list)
                # print self.get_scan()
            return True
        else:
            if self.get_scan() is not None:
                self.set_scan(subtitles_list=None)
            return False

    def extract_subtitles(self, file_source=None):
        """
        Extract subtitles

        """
        if file_source is None:
            if self.get_mks_file_path() is None:
                raise SystemError('"Impossible to make thing without a file source."')
            else:
                file_source = self.get_mks_file_path()

        if type(file_source) != str:
            raise TypeError('"file_source" must be a str type')

        if self.get_scan():
            # a small header
            self._display_subtitles_extraction_header()

            # For each track found during the scan
            for subtitle_track in self.get_scan():

                # display a summary for the extracted track
                self._display_subtitles_extraction_summary_by_track(subtitles_track=subtitle_track)

                # start subprocess
                proc = subprocess.Popen(
                    self.get_cmd_for_extract_subtitle(
                        file_source=file_source,
                        subtitle_track_info=subtitle_track),
                    stdout=subprocess.PIPE
                )

                # set non-blocking mode
                outfile = proc.stdout
                outfd = outfile.fileno()
                file_flags = fcntl.fcntl(outfd, fcntl.F_GETFL)
                fcntl.fcntl(outfd, fcntl.F_SETFL, file_flags | os.O_NDELAY)

                # use 'select' for reading
                while True:
                    ready = select.select([outfd], [], [])  # wait for input
                    if outfd in ready[0]:
                        outchunk = outfile.read()
                        if not outchunk:
                            break
                        else:
                            if self.enable_progress_bar:
                                # remove \r and \n is important for the rest of match
                                outchunk = re.sub('\r', '', outchunk)
                                outchunk = re.sub('\n', '', outchunk)
                                if re.search('^[a-zA-Z0-9]+\s:\s\d+|\d%$', outchunk):
                                    percentage = re.findall('(\d+|\d)', outchunk)
                                    rows, columns = os.popen('stty size', 'r').read().split()

                                    self.utils.cli_progress_bar(
                                        'Extracting: ',
                                        int(percentage[0]),
                                        100,
                                        int(columns)
                                    )
                # Clear the Text Progress Bar line
                if self.enable_progress_bar:
                    sys.stdout.write("\x1b[2K")
                    sys.stdout.write("\r")
                    sys.stdout.flush()

    def check_forced_flag(self):
        """
        Look every extracted files and for each languages found the traks it have the smaller size.
        The smaller track of each language will be rename with .Flag
        The bigger will be the default
        The rest will revive a un-understandable name for kodi

        """
        sys.stdout.write('Check the Forced track ...\n')
        sys.stdout.flush()

        self.subtitles_it_have_default = list()
        self.subtitles_it_have_forced = list()
        represented_language = list()

        for tracks in self.get_scan():
            if not tracks['Language'] in represented_language:
                represented_language.append(tracks['Language'])

        for language in represented_language:
            sys.stdout.write('For ' + language + ':\n')
            little_size = None
            track_it_have_little_size = None
            bigger_size = None
            track_it_have_bigger_size = None
            for tracks in self.get_scan():
                if tracks['Language'] == language:
                    subtitles_filename = self.get_actual_subtitle_filename(
                        title=self.utils.get_title_from_video_file(self.get_mks_file_path()),
                        subtitle_track_info=tracks
                    )

                    subtitles_file = os.path.join(self.get_working_directory(), subtitles_filename)

                    if os.path.isfile(subtitles_file):
                        subtitles_file_size = os.path.getsize(subtitles_file)

                    # Check little size
                    if little_size is None:
                        little_size = subtitles_file_size
                        track_it_have_little_size = tracks['ID']
                    elif subtitles_file_size <= little_size:
                        little_size = subtitles_file_size
                        track_it_have_little_size = tracks['ID']

                    # Check bigger size
                    if bigger_size is None:
                        bigger_size = subtitles_file_size
                        track_it_have_bigger_size = tracks['ID']
                    elif subtitles_file_size >= bigger_size:
                        bigger_size = subtitles_file_size
                        track_it_have_bigger_size = tracks['ID']

                    sys.stdout.write(' ')
                    sys.stdout.write('Track:')
                    sys.stdout.write(str(tracks['ID']))
                    sys.stdout.write(' ')
                    sys.stdout.write('Size: ')
                    sys.stdout.write(str(subtitles_file_size))
                    sys.stdout.write(' ')
                    sys.stdout.write('Path:')
                    sys.stdout.write(subtitles_file)
                    sys.stdout.write('\n')
                    sys.stdout.flush()
            if track_it_have_little_size is not None:
                self.subtitles_it_have_forced.append(track_it_have_little_size)
                sys.stdout.write('Candidate to be Forced ')
                sys.stdout.write('track number ')
                sys.stdout.write(str(track_it_have_little_size))
                sys.stdout.write('\n')
                sys.stdout.flush()
            if track_it_have_bigger_size is not None:
                self.subtitles_it_have_default.append(track_it_have_bigger_size)
                sys.stdout.write('Candidate to be Default ')
                sys.stdout.write('track number ')
                sys.stdout.write(str(track_it_have_bigger_size))
                sys.stdout.write('\n')
                sys.stdout.flush()

    def rename_subtitles_files(self):
        """
        It function is in charge to rename extracted subtitles files, to Kodi naming format, and let unknow tracks
        ready to be analyze by a human.
        """
        if self.get_scan() and self.get_mkv_file_path() and self.get_working_directory():
            # display a small header
            self._display_subtitles_rename_files_header()
            for track in self.get_scan():

                actual_subtitles_filename = self.get_actual_subtitle_filename(
                    title=self.utils.get_title_from_video_file(self.get_mks_file_path()),
                    subtitle_track_info=track
                )

                actual_subtitles_file = os.path.join(self.get_working_directory(), actual_subtitles_filename)

                if track['ID'] in self.subtitles_it_have_default or track['ID'] in self.subtitles_it_have_forced:
                    desired_subtitle_filename = self.get_desired_subtitle_filename(
                        title=self.utils.get_title_from_video_file(self.get_mks_file_path()),
                        subtitle_track_info=track
                    )

                    desired_subtitle_file = os.path.join(self.get_working_directory(), desired_subtitle_filename)

                    self._display_subtitles_rename_files_info(actual_subtitles_file, desired_subtitle_file, track)

                    try:
                        os.rename(actual_subtitles_file, desired_subtitle_file)
                        if os.path.isfile(
                                os.path.join(
                                    self.get_working_directory(),
                                    os.path.basename(os.path.splitext(actual_subtitles_file)[0] + '.idx')
                                )):
                            os.rename(
                                os.path.join(
                                    self.get_working_directory(),
                                    os.path.basename(os.path.splitext(actual_subtitles_file)[0] + '.idx')),
                                os.path.join(
                                    self.get_working_directory(),
                                    os.path.basename(os.path.splitext(desired_subtitle_file)[0] + '.idx'))

                            )
                    except OSError:
                        sys.stdout.write('Error: failed to rename, extract have probably make a error...\n')
                    sys.stdout.flush()

    def get_desired_subtitle_filename(self, title=None, subtitle_track_info=None):
        """
        Return the desired filename of the subtitle by look information's pass as argument.
        that mean consider Language and Forced status, and add it in the return filename.

        The naming standard use is the kodi one.

        The title is suppose to be the Video Title like 'XMass 2017', the title can be pass as argument or the function
        will try to use SubRipper.utils.get_title_from_video_file() with the return of SubRipper.get_mks_file_path() as
        argument.

        :param title: The video title
        :type title: str
        :param subtitle_track_info: the track information supose to be a list item from SubRipper.get_scan()
        :type subtitle_track_info: dict
        :return: The desired subtitle filename or None if can't
        :rtype: str or None
        :raise SystemError: When title is None and SubRipper.get_mks_file_path() return None
        :raise TypeError: When title is not a str
        """
        if title is None:
            if self.get_mks_file_path() is None:
                raise SystemError('"Impossible to make thing without a title."')
            else:
                title = self.utils.get_title_from_video_file(self.get_mks_file_path())

        if type(title) != str:
            raise TypeError('"title" must be a str type')

        if subtitle_track_info is not None:
            try:
                desired_subtitles_filename = ''
                desired_subtitles_filename += title
                desired_subtitles_filename += '.'
                desired_subtitles_filename += str(subtitle_track_info['Language'])
                if subtitle_track_info['ID'] in self.subtitles_it_have_forced:
                    if subtitle_track_info['ID'] not in self.subtitles_it_have_default:
                        desired_subtitles_filename += '.Forced'
                desired_subtitles_filename += self.get_subtitle_extension_file(codec=subtitle_track_info['CodecID'])
                return desired_subtitles_filename
            except KeyError:
                return None
        else:
            return None

    def get_actual_subtitle_filename(self, title=None, subtitle_track_info=None):
        """
        Return the filename of the subtitle by look information's pass as argument.

        The title is suppose to be the Video Title like 'XMass 2017', the title can be pass as argument or the function
        will try to use SubRipper.utils.get_title_from_video_file() with the return of SubRipper.get_mks_file_path() as
        argument.

        :param title: The video title
        :type title: str
        :param subtitle_track_info: the track information supose to be a list item from SubRipper.get_scan()
        :type subtitle_track_info: dict
        :return: The subtitle filename or None if can't
        :rtype: str or None
        :raise SystemError: When title is None and SubRipper.get_mks_file_path() return None
        :raise TypeError: When title is not a str
        """
        if title is None:
            if self.get_mks_file_path() is None:
                raise SystemError('"Impossible to make thing without a title."')
            else:
                title = self.utils.get_title_from_video_file(self.get_mks_file_path())

        if type(title) != str:
            raise TypeError('"title" must be a str type')

        if subtitle_track_info is not None:
            try:
                actual_subtitle_filename = ''
                actual_subtitle_filename += title
                actual_subtitle_filename += '.'
                actual_subtitle_filename += str(subtitle_track_info['Language'])
                actual_subtitle_filename += '_'
                actual_subtitle_filename += str(subtitle_track_info['RandomID'])
                actual_subtitle_filename += self.get_subtitle_extension_file(codec=subtitle_track_info['CodecID'])
                return actual_subtitle_filename
            except KeyError:
                return None
        else:
            return None

    def get_mkvextract_mode_track_line(self, file_source=None, working_directory=None, subtitle_track_info=None):
        """
        For be conform to MKVExtract syntax like:
        mkvextract tracks <your_mkv_video> <track_number>:<subtitle_file.srt>

        It function return the <track_number>:<subtitle_file.srt> section, by get every information's from the
        SubRipper class object and the dictionary pass as argument.

         dict['ID'], dict['Language'], dict['RandomID'], dict['CodecID'] are require for it function
          ID: is the ID -1 found via mediainfo
          Language: is the lang found ins the original MKV file, and lookup by pycountry for a full text language.
          RandomID: is a unique ID generate by Utils.get_new_id()

        :param file_source: the source file from where extract subtitles
        :type file_source: str as return by os.path.abspath()
        :param working_directory: the optional working directory or it will use get_working_directory() return
        :type working_directory: str as return by os.path.abspath(os.path.dirname(file_source))
        :param subtitle_track_info: the track as store in scan list
        :type subtitle_track_info: dict or None
        :return: the argument line ready to use by mkvextract or None if dict is not complete or set to None
        :rtype: str or None
        :raise SystemError: file_source is None and get_mks_file_path() return None
        :raise TypeError: when file_source is not a str as return by os.path.abspath()
        """

        if file_source is None:
            if self.get_mks_file_path() is None:
                raise SystemError('"Impossible to make thing without a file source."')
            else:
                file_source = self.get_mks_file_path()

        if type(file_source) != str:
            raise TypeError('"file_source" must be a str type')

        if working_directory is None:
            if self.get_working_directory() is None:
                working_directory = os.path.abspath(os.path.dirname(file_source))
            else:
                working_directory = self.get_working_directory()

        if subtitle_track_info is not None:
            # mkvextract tracks <your_mkv_video> <track_number>:<subtitle_file.srt>
            try:
                line_to_return = ''
                line_to_return += str(subtitle_track_info['ID'])
                line_to_return += ':'
                line_to_return += working_directory
                line_to_return += os.path.sep
                line_to_return += self.utils.get_title_from_video_file(file_source)
                line_to_return += '.'
                line_to_return += str(subtitle_track_info['Language'])
                line_to_return += '_'
                line_to_return += str(subtitle_track_info['RandomID'])
                line_to_return += self.get_subtitle_extension_file(
                    codec=subtitle_track_info['CodecID']
                )
                return line_to_return
            except KeyError:
                return None
        else:
            return None

    def get_cmd_for_extract_subtitle(self, file_source=None, subtitle_track_info=None):
        """
        That function use mkvextract and are supposed to be call in loop foreach Scanned subtitles tracks,
        Nothing magic the goal is to return command line for extract a individual track.

        That function need a source file, from where extract subtitles, and a dictionary pass as argument
        with information's about the track to extract.

        That track information will be forward to SubRipper.get_mkvextract_mode_track_line() function from where
        The magic happen.

        The returned value is a list , where each item are a command line and argument.


        :param file_source: the source file from where extract subtitles track
        :type file_source: str
        :param subtitle_track_info: the dictionary it contain track information
        :type subtitle_track_info: dict
        :return: the command line follow by command line argument's
        :rtype: list
        :raise SystemError: when mkvextract can't be found
        :raise SystemError: when file_source is None and get_mks_path() return None
        :raise TypeError: when file source is not a str as return by os.path.abspath()
        :raise IOError: when file source can't be read
        :raise TypeError: when subtitles_track is not a dictionary type
        """
        # Exit as soon of possible
        if self.utils.get_mkvextract_path() is None:
            # unfortunately can't be test
            raise SystemError('"mkvextract" can\'t be found, install it before retry...')

        if file_source is None:
            if self.get_mks_file_path() is None:
                raise SystemError('"Impossible to make thing without a file source."')
            else:
                file_source = self.get_mks_file_path()

        if type(file_source) != str:
            raise TypeError('"file_source" must be a str type')

        if type(subtitle_track_info) != dict:
            raise TypeError('"subtitle_track_info" is not a a dictionary type')

        # test read of file source
        if not os.access(os.path.abspath(file_source), os.O_RDWR):
            raise IOError('"' + str(file_source) + '" can\'t be read')

        # get a dedicated mkvextract line via subtitle_track_info dictionary
        track_number_dot_subtitles_file = self.get_mkvextract_mode_track_line(
            file_source=file_source,
            subtitle_track_info=subtitle_track_info
        )

        # everything look ok we can continue
        command = list()
        command.append(unicode(self.utils.get_mkvextract_path(), 'utf-8'))
        command.append(unicode('tracks', 'utf-8'))
        command.append(unicode(file_source, 'utf-8'))
        command.append(unicode(track_number_dot_subtitles_file, 'utf-8'))

        # finally return the command
        return command

    def get_cmd_for_generate_a_mks_file(self, file_source=None, file_destination=None):
        """
        Return the command line for generate a MKS file, subtitles.

        The goal to create a MKS file is to remove everything exempt SubTitles before extract them.
        mkvextract have better result , and that limit confusion about track ID.

        That clearly the originality of SubRipper, that function have been imagine from the begging of the project.

        The command line is store in a list and constitute by:
         mkvmerge absolute path
         --output
         absolute path of the file_destination
         --no-audio
         --no-video
         --no-buttons
         --no-attachments
         --no-chapters
         absolute path of the file_source

        All that stuff is store inside a list type, where each argument's is a list item.
        That is for be ready to be use by SubProcess module.

        For prevent to return a command it can't be execute on a system without error,
        that function try to crash as soon of possible. that the rude way by impossible to haven't a defensive
        position from here.

        :param file_source: a absolute path of a source file
        :type file_source: str
        :param file_destination: a absolute path of a destination file
        :type file_destination: str
        :return: command follow by arguments inside a list type
        :rtype: list
        :raise SystemError: when mkvmerge can't be found
        :raise IOError: when file_source is not a valid existing file
        :raise IOError: when file_destination can't be create on the file system
        :raise TypeError: when file_source is not a str as return by os.path.abspath(file_source)
        :raise TypeError: when file_destination is not a str as return by os.path.abspath(file_destination)
        """
        # Exit as soon of possible
        if self.utils.get_mkvmerge_path() is None:
            # unfortunately can't be test
            raise SystemError('"mkvmerge" can\'t be found, install it before retry...')
        if type(file_source) != str:
            raise TypeError('"file_source" must be str type...')
        if type(file_destination) != str:
            raise TypeError('"file_destination" must be str type...')
        if not os.path.isfile(os.path.abspath(file_source)):
            raise IOError('"' + str(file_source) + '" don\'t exist or is not a file')
        # ho year ! a write test on fly or die, what else ?
        try:
            open(os.path.abspath(file_destination), 'w').close()
            os.remove(os.path.abspath(file_destination))
        except IOError:
            raise IOError('"' + str(os.path.abspath(file_destination)) + '" Permission denied')

        # Everything look OK we can continue
        # create a list of argument ready for subprocess module.
        command = list()
        command.append(unicode(self.utils.get_mkvmerge_path(), 'utf-8'))
        command.append(unicode('--output', 'utf-8'))
        command.append(unicode(os.path.abspath(file_destination), 'utf-8'))
        command.append(unicode('--no-audio', 'utf-8'))
        command.append(unicode('--no-video', 'utf-8'))
        command.append(unicode('--no-buttons', 'utf-8'))
        command.append(unicode('--no-attachments', 'utf-8'))
        command.append(unicode('--no-chapters', 'utf-8'))
        command.append(unicode(os.path.abspath(file_source), 'utf-8'))

        # finally return the cmd
        return command

    def get_cmd_for_scan_subtitles(self, file_source=None, mediainfo_path=None):
        """
        Return the command line for extract subtitles.

        It is constitute by mediainfo absolute path, a long line for have impose a output format easy to parse
        and a absolute path of a source file.

        All that stuff is store inside a list type, where each argument's is a list item.

        :param file_source: absolute path to the file to scan
        :type file_source: str
        :param mediainfo_path: absolute path of mediainfo or None to find mediainfo automatically.
        :type mediainfo_path: str or None
        :return: command follow by arguments
        :rtype: list
        :raise SystemError: when file_to_scan is None
        :raise SystemError: when mediainfo can't be found
        """
        # Exit as soon of possible
        if file_source is None:
            raise SystemError('"file_to_scan" can\'t  be None')
        if mediainfo_path is None:
            if self.utils.get_mediainfo_path() is None:
                raise SystemError('"mediainfo" can\'t be found, install it before retry...')
            else:
                mediainfo_path = self.utils.get_mediainfo_path()
        else:
            if self.utils.which(str(mediainfo_path)):
                mediainfo_path = self.utils.which(mediainfo_path)
            else:
                raise SystemError('"mediainfo" can\'t be found, verify you setting...')

        # Everything look ok we can continue
        # Start by create command list
        command = list()
        command.append(unicode(mediainfo_path, 'utf-8'))
        command.append(unicode(self.scan_subtitles_detect_line, 'utf-8'))
        command.append(unicode(file_source, 'utf-8'))

        # finally return the command
        return command

    @staticmethod
    def get_subtitle_extension_file(codec=None):
        """
        Return a subtitle extension file, like a string it contain '.srt', it's use for create
        the extracted subtitles filename.

        S_TEXT/UTF8 Simple text subtitles will be written as SRT files.
        S_TEXT/SSA  SSA text subtitles will be written as SSA files.
        S_TEXT/ASS 	ASS text subtitles will be written as ASS files.
        S_KATE 	    Kate(tm) streams will be written within an Ogg(tm) container.
        S_VOBSUB 	VobSub(tm) subtitles will be written as SUB files along with the respective index files, as IDX files.
        S_TEXT/USF 	USF text subtitles will be written as USF files.
        S_HDMV/PGS 	PGS subtitles will be written as SUP files.

        :return: a string like '.srt' or '.ass' or '.sub' it depend of the 'codec' pass as argument
        :rtype: str
        """
        if codec is None:
            return '.srt'
        elif codec.upper() == 'S_TEXT/UTF8':
            return '.srt'
        elif codec.upper() == 'S_TEXT/SSA':
            return '.ssa'
        elif codec.upper() == 'S_TEXT/ASS':
            return '.ass'
        elif codec.upper() == 'S_KATE':
            return '.ogg'
        elif codec.upper() == 'S_VOBSUB':
            return '.sub'
        elif codec.upper() == 'S_TEXT/USF':
            return '.usf'
        elif codec.upper() == 'S_HDMV/PGS':
            return '.sup'
        else:
            return '.srt'

    def set_scan(self, subtitles_list=None):
        """
        For be testable or if you have idea about how use it , you can flush the scan attribute, by the list you want.

        :param subtitles_list: a list you want set to the scan attribute
        :type subtitles_list: list or None
        :raise TypeError: when subtitles_list is not a list
        """
        # try to crash as soon of possible
        if subtitles_list is None:
            subtitles_list = list()
        if type(subtitles_list) != list:
            raise TypeError('"subtitles_list" must be a list type')
        # everything look ok
        if self.get_scan() != subtitles_list:
            self.scanned_subtitles_list = subtitles_list

    def get_scan(self):
        """
        It function return the scan attribute, suppose to store a list where each item contain a dictionary

        :return: the scan attribute
        :rtype: list
        """
        return self.scanned_subtitles_list

    def set_working_directory(self, directory=None):
        """
        Set the working directory where will be save, extracted subtitles.
        The directory must exist, and have write permission.

        Note: subripper.set_working_directory() is call by subripper.set_file(), then if you want to change the
        working directory you must do it after a call of subripper.set_file().

        By default subripper.set_file() will alway set the working directory to the base directory of the source file.

        :param directory: the directory
        :type directory: str or None
        :raise SystemError: when the directory don't exist
        :raise SystemError: when the directory haven't write permission
        """
        if directory is not None:
            if self.get_working_directory() != os.path.realpath(directory):
                self.working_directory = os.path.realpath(directory)
        else:
            if self.get_mkv_file_path() is None:
                if self.get_working_directory() is not None:
                    self.working_directory = None
            else:
                if self.get_working_directory() != os.path.dirname(os.path.abspath(self.get_mkv_file_path())):
                    self.working_directory = os.path.dirname(os.path.abspath(self.get_mkv_file_path()))

    def get_working_directory(self):
        """
        Return the absolute working directory path where save extracted subtitles

        :return: filename path
        :rtype: str
        """
        return self.working_directory

    def set_mkv_file_path(self, filename=None):
        """
        Set the filename, care about the path it will be estimate

        By set 'filename' to None , the working directory will be set to None.
        When you use SubRipper.set_file() , the working directory value will update , the root directory where
        is store the 'filename'.

        If you want export you subtitles somewhere else use SubRipper.set_working_directory() after a
        SubRipper.set_file() call.

        :param filename: the filename to set
        :type filename: str or None
        :raise SystemError: when 'filename' is not None and not a true existing and readable file.
        """
        if filename is not None:
            if os.path.isfile(os.path.realpath(filename)):
                if self.get_mkv_file_path() != os.path.realpath(filename):
                    self.input_file = os.path.abspath(filename)
                    self.set_working_directory(os.path.dirname(os.path.abspath(filename)))
            else:
                raise SystemError('"' + str(filename) + '" is not a true existing and readable file')
        else:
            self.input_file = None
            self.set_working_directory(None)

    def get_mkv_file_path(self):
        """
        Return the absolute filename path from where extract subtitles

        :return: file path
        :rtype: str or None
        """
        return self.input_file

    def get_mks_file_path(self):
        """
        Return a valid mks file name, by use Utils.get_file() and Utils.get_working_directory()

        The function will return the absolute path of a none existent file or None if subripper.get_working_directory
        or subripper.get_file() return None

        :return: absolute path
        :rtype: str or None
        """
        if self.get_mkv_file_path() is None or self.get_working_directory() is None:
            return None
        else:
            file_mks = ''
            file_mks += self.get_working_directory()
            file_mks += os.path.sep
            file_mks += self.utils.get_title_from_video_file(self.get_mkv_file_path())
            file_mks += '.mks'

            return file_mks

    def set_name(self, app_name=None):
        """
        Set a name store in the name attribute. You can identify you class by set a name and use subripper.get_name()
        That function is use by the init, and you normally never need to change the name.

        :param app_name: the name to set
        :type app_name: str
        :raise TypeError: when name is not a str
        """
        if app_name is None:
            app_name = self.__class__.__name__
        if type(app_name) != str:
            raise TypeError('"name" must be a str type')
        if self.get_name() != app_name:
            self.name = app_name

    def get_name(self):
        """
        Return the name of the SubRipper, useful for --version by example or if you want identify you class by name.

        The default value is the class name, like __class__.__name__

        :return: the name
        :rtype: str
        """
        return self.name

    def set_version(self, value=0.5):
        """
        Set the version number, you normally never have to set the version ;-)

        :param value: the value
        :type value: float
        :raise TypeError: when value is not a float type
        """
        # Try to crash as soon of possible
        if type(value) != float:
            raise TypeError('"value" must be a float type')

        # Everything look fine we can continue
        if self.get_version() != value:
            self.version = value

    def get_version(self):
        """
        Return the version number, it consist to a float number like '1.0', it function is use by -V or --version

        :return: the version number
        :rtype: float
        """
        return self.version

    def _display_header(self):
        """
        A show function, for display program dependency name and version.

        Indirectly that function call all tests for all dependency, then if something is display , all
        dependency's are cover
        """
        sys.stdout.write('' + self.get_name() + ' v' + str(self.get_version()) + ', ')
        sys.stdout.write('' + self.utils.get_mediainfo_version() + ', ')
        sys.stdout.write('' + self.utils.get_mkvextract_version())
        sys.stdout.write('\n')
        sys.stdout.flush()

    @staticmethod
    def _display_subtitles_rename_files_header():
        """Display a header before rename subtitle's files"""
        sys.stdout.write('Rename SubTitles:')
        sys.stdout.write('\n')
        sys.stdout.flush()

    @staticmethod
    def _display_subtitles_rename_files_info(actual_subtitles_file, desired_subtitles_file, track):
        sys.stdout.write('Renaming Track: ')
        sys.stdout.write(str(track['ID']))
        sys.stdout.write('\n')
        sys.stdout.write(' Src: ')
        sys.stdout.write(actual_subtitles_file)
        sys.stdout.write('\n')
        sys.stdout.write(' Dst: ')
        sys.stdout.write(desired_subtitles_file)
        sys.stdout.write('\n')

    def _display_summary_generate_mks_file(self):
        sys.stdout.write('Generate a MKS file: ')
        sys.stdout.write('\n')
        if self.get_working_directory():
            sys.stdout.write(' Working Directory: ' + self.get_working_directory() + '/\n')
        else:
            sys.stdout.write(' Working Directory: ' + 'None' + '\n')
        if self.get_mkv_file_path():
            sys.stdout.write(' Source File      : ' + self.get_mkv_file_path() + '\n')
        else:
            sys.stdout.write(' Source File      : ' + 'None' + '\n')
        if self.get_mks_file_path():
            sys.stdout.write(' Destination file : ' + self.get_mks_file_path() + '\n')
        else:
            sys.stdout.write(' Destination file : ' + 'None' + '\n')
        sys.stdout.flush()

    def _display_summary_after_scan(self):
        if self.get_scan():
            if len(self.get_scan()) == 1:
                sys.stdout.write('1 subtitle found:\n')
                sys.stdout.flush()
            else:
                sys.stdout.write(str(len(self.get_scan())) + ' subtiles founds:\n')
                sys.stdout.flush()
            for subtitles in self.get_scan():
                sys.stdout.write(' ')
                sys.stdout.write('ID: ' + str(subtitles['ID']))
                sys.stdout.write(', ')
                sys.stdout.write('Language: ' + subtitles['Language'])
                sys.stdout.write(', ')
                sys.stdout.write('Default: ' + subtitles['Default'])
                sys.stdout.write(', ')
                sys.stdout.write('Forced: ' + subtitles['Forced'])
                sys.stdout.write(', ')
                sys.stdout.write('Format: ' + subtitles['Format'])
                sys.stdout.write(', ')
                sys.stdout.write('CodecID: ' + subtitles['CodecID'])
                sys.stdout.write(', ')
                sys.stdout.write('CodecInfo: ' + subtitles['CodecInfo'])
                sys.stdout.write('\n')
                sys.stdout.flush()

    def _display_subtitles_extraction_header(self):
        if self.get_scan():
            # Small header
            if len(self.get_scan()) == 1:
                sys.stdout.write('Extraction of 1 subtitle track:')
            if len(self.get_scan()) > 1:
                sys.stdout.write('Extraction of')
                sys.stdout.write(' ')
                sys.stdout.write(str(len(self.get_scan())))
                sys.stdout.write(' ')
                sys.stdout.write('subtitles tracks:')

            sys.stdout.write('\n')
            sys.stdout.flush()

    def _display_subtitles_extraction_summary_by_track(self, subtitles_track=None):
        if subtitles_track is not None:
            if self.get_scan():
                sys.stdout.write('Track: ')
                sys.stdout.write(str(subtitles_track['ID']))
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Language: ')
                sys.stdout.write(str(subtitles_track['Language']))
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Format: ')
                sys.stdout.write(str(subtitles_track['Format']))
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Codec: ')
                sys.stdout.write(str(subtitles_track['CodecID']))
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Codec Info: ')
                sys.stdout.write(str(subtitles_track['CodecInfo']))
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Filename: ')
                sys.stdout.write(self.utils.get_title_from_video_file(self.get_mkv_file_path()))
                sys.stdout.write('.' + str(subtitles_track['Language']))
                sys.stdout.write('_' + str(subtitles_track['RandomID']))
                if subtitles_track['Forced'] == 'Yes':
                    sys.stdout.write('.Forced')
                sys.stdout.write(self.get_subtitle_extension_file(codec=subtitles_track['CodecID']))
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Working Directory: ')
                sys.stdout.write(self.get_working_directory() + os.path.sep)
                sys.stdout.write('\n')

                sys.stdout.write(' ')
                sys.stdout.write('Random ID: ')
                sys.stdout.write(str(subtitles_track['RandomID']))
                sys.stdout.write('\n')

                # finally flush the stdout
                sys.stdout.flush()

    @staticmethod
    def _display_nothing_do_do():
        sys.stdout.write('Nothing to do ...')
        sys.stdout.write('\n')
        sys.stdout.flush()


def main():
    version = 0.5
    name = 'SubRipper'
    enable_progress = False
    debug_level = 0

    def check_file(file_to_check):
        if not os.path.isfile(file_to_check):
            raise argparse.ArgumentTypeError(
                '"{0}" is not a file'.format(file_to_check)
            )
        elif not os.access(file_to_check, os.R_OK):
            raise argparse.ArgumentTypeError(
                'File "{0}" can\'t be read'.format(file_to_check)
            )
        # everything look ok
        return file_to_check

    parser = argparse.ArgumentParser(
        description="GalaxieDrake SubRipper script",
        prog=name,
        version='{0} v{1}  - GalaxieDrake subtitles ripper script'.format(name, version),
        usage='%(prog)s [options] filename\n'
              'Invoke it script with the path of a file\n'
              'Every subtitles will be extract inside the base directory\n'
              'Few Examples:\n'
              ' %(prog)s ./file.mkv\n'
              ' %(prog)s ../a/other/file.mkv\n'
              ' %(prog)s /full/path/to/the/file.mkv\n'
              ' %(prog)s \"/full/path/to/the/directory/File With space.mkv/\"\n'
    )
    parser.add_argument('-d', '--debug',
                        action="count",
                        default=0,
                        help="Increase output verbosity, -d or -dd or -ddd are accepted"
                        )
    parser.add_argument('-P', '--progress',
                        action='store_true',
                        help='This option tells %(prog)s to print information showing the progress of the scan. '
                             'This gives a bored user something to watch.'
                        )
    parser.add_argument('filename',
                        type=check_file,
                        action="store",
                        help='File from where extract subtitles',
                        metavar='filename'
                        )

    if sys.argv[0] == 'python -m unittest':
        args = parser.parse_args([])
    else:
        args = parser.parse_args()

    if args.progress:
        enable_progress = args.progress

    if args.debug:
        debug_level = args.verbosity

    if args.filename:
        p = SubRipper(
            filename=args.filename,
            enable_progress=enable_progress,
            verbose=debug_level,
            app_version=version
        )
        p.run()

    return 0


if __name__ == "__main__":
    sys.exit(main())
