#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: Jérôme ORNECH alias "Tuux" <tuxa@rtnp.org> all rights reserved

from GalaxieDrake.utils import Utils
from GalaxieDrake.taskspooler import TaskSpooler

__author__ = u"Jérôme Ornech"
__copyright__ = u"Copyright 2016-2017, The Galaxie Curses Project"
__credits__ = [u"Jérôme Ornech alias Tuux", u"Aurélien Maury alias Mo"]
__license__ = u"GNU GENERAL PUBLIC LICENSE 3.0"
__version__ = u"0.2"
__maintainer__ = u"Jerome Ornech"
__email__ = u"tuux at rtnp dot org"
__status__ = u"Development"


