#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: "Tuux" <tuxa@rtnp.org> all rights reserv

import unittest
import sys
import os
import re
import contextlib
import time

from GalaxieDrake.taskspooler import TaskSpooler


@contextlib.contextmanager
def capture():
    from cStringIO import StringIO
    oldout, olderr = sys.stdout, sys.stderr
    try:
        out = [StringIO(), StringIO()]
        sys.stdout, sys.stderr = out
        yield out
    finally:
        sys.stdout, sys.stderr = oldout, olderr
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


# Unittest
class TestSubRipper(unittest.TestCase):
    def test_get_add_job_cmd(self):
        """Test TaskSpooler.get_add_job()"""
        taskspooler = TaskSpooler()
        command = list()
        command.append(unicode("ls", 'utf-8'))
        command.append(unicode("-lah", 'utf-8'))

        self.assertEqual(
            [u'/usr/bin/tsp', u'-E', u'-g', u'-m', u'-d', u'-D', u'42', u'-L', u'Galaxie', u'-N', u'1', u'ls', u'-lah'],
            taskspooler.get_add_job_cmd(
                command=command,
                output=True,
                stderr=True,
                gzip=True,
                fork=True,
                send_email=True,
                check_job_before=True,
                check_job_id=42,
                label='Galaxie',
                slot=1
            ))

    def test_add_job(self):
        """Test TaskSpooler.add_transcoder_job()"""
        taskspooler = TaskSpooler()
        command = list()
        command.append(unicode("ls", 'utf-8'))
        command.append(unicode("-lah", 'utf-8'))

        taskspooler.kill_the_task_spooler_server()
        self.assertEqual(0, taskspooler.add_job(command=command))
        self.assertEqual(1, taskspooler.add_job(command=command))
        self.assertEqual(2, taskspooler.add_job(command=command))

        # test raise error
        self.assertRaises(TypeError, taskspooler.add_job, command=command, output='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, stderr='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, gzip='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, fork='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, send_email='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, check_job_before='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, check_job_id='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=command, label=float(42))
        self.assertRaises(TypeError, taskspooler.add_job, command=command, slot='Galaxie')
        self.assertRaises(TypeError, taskspooler.add_job, command=float(42))

    def test_check_for_taskspooler(self):
        """Test TaskSpooler.check_for_taskspooler()"""
        taskspooler = TaskSpooler()
        self.assertEqual(type(taskspooler.check_for_taskspooler()), type(str()))
        self.assertTrue(os.path.isfile(taskspooler.check_for_taskspooler()))
        self.assertRaises(SystemError, taskspooler.check_for_taskspooler, app_name='Galaxie42')

    def test_get_path(self):
        """Test TaskSpooler.get_path()"""
        taskspooler = TaskSpooler()
        self.assertEqual(type(taskspooler.get_path()), type(str()))
        self.assertTrue(os.path.isfile(taskspooler.get_path()))

    def test_kill_the_task_spooler_server(self):
        """Test TaskSpooler.kill_the_task_spooler_server()"""
        taskspooler = TaskSpooler()
        self.assertEqual(None, taskspooler.kill_the_task_spooler_server())

    def test_clear_the_list_of_finished_jobs(self):
        """Test TaskSpooler.clear_the_list_of_finished_jobs()"""
        taskspooler = TaskSpooler()
        self.assertEqual(None, taskspooler.clear_the_list_of_finished_jobs())

    def test_show_the_job_list(self):
        """Test TaskSpooler.show_the_job_list()"""
        taskspooler = TaskSpooler()
        with capture() as output:
            taskspooler.show_the_job_list()

            # self.assertEqual('', output[0].strip())

    def test_get_jobs_list(self):
        """Test TaskSpooler.get_jobs_list()"""
        taskspooler = TaskSpooler()
        self.assertEqual(type(taskspooler.get_jobs_list()), type(list()))

    def test_set_get_number_of_simultaneous_jobs(self):
        """Test TaskSpooler.get_number_of_simultaneous_jobs()"""
        taskspooler = TaskSpooler()
        self.assertEqual(type(int()), type(taskspooler.get_number_of_simultaneous_jobs()))
        # set the value to 42
        taskspooler.set_number_of_simultaneous_jobs(42)
        self.assertEqual(42, taskspooler.get_number_of_simultaneous_jobs())
        # back to default value
        taskspooler.set_number_of_simultaneous_jobs()
        self.assertEqual(1, taskspooler.get_number_of_simultaneous_jobs())
        # test raise error
        self.assertRaises(TypeError, taskspooler.set_number_of_simultaneous_jobs, 'Galaxie')

    def test_get_pid(self):
        """Test TaskSpooler.get_pid()"""
        taskspooler = TaskSpooler()
        command = list()
        command.append(unicode("ls", 'utf-8'))
        command.append(unicode("-lah", 'utf-8'))

        job_id = taskspooler.add_job(command=command)
        self.assertEqual(taskspooler.get_pid(job_id), taskspooler.get_pid())
        try:
            self.assertEqual(type(int()), type(taskspooler.get_pid(job_id)))
        except AssertionError:
            self.assertEqual(None, taskspooler.get_pid(job_id))

        # if job not exist return None
        self.assertEqual(None, taskspooler.get_pid(42))
        taskspooler.kill_the_task_spooler_server()
        self.assertEqual(None, taskspooler.get_pid())

        # test error raise
        taskspooler.add_job(command=command)
        self.assertRaises(TypeError, taskspooler.get_pid, float(42))

    def test_get_output_file(self):
        """Test TaskSpooler.get_output_file()"""
        taskspooler = TaskSpooler()
        command = list()
        command.append(unicode("ls", 'utf-8'))
        command.append(unicode("-lah", 'utf-8'))

        job_id = taskspooler.add_job(command=command)
        # self.assertEqual(taskspooler.get_output_file(job_id), taskspooler.get_output_file())

        try:
            self.assertEqual(type(str()), type(taskspooler.get_output_file(job_id)))
        except AssertionError:
            self.assertEqual(None, taskspooler.get_output_file(job_id))

        # if job not exist return None
        self.assertEqual(None, taskspooler.get_output_file(42))
        taskspooler.kill_the_task_spooler_server()
        self.assertEqual(None, taskspooler.get_output_file())

        # test error raise
        taskspooler.add_job(command=command)
        self.assertRaises(TypeError, taskspooler.get_output_file, float(42))

    def test_get_job_information(self):
        """Test TaskSpooler.get_job_information()"""
        taskspooler = TaskSpooler()
        command = list()
        command.append(unicode("sleep", 'utf-8'))
        command.append(unicode("3", 'utf-8'))
        job_id = taskspooler.add_job(command=command)

        self.assertEqual(type(dict()), type(taskspooler.get_job_information()))
        self.assertEqual(8, len(taskspooler.get_job_information()))
        returned_vale = taskspooler.get_job_information(job_id)
        for key in returned_vale:
            self.assertTrue(key in ['Exit_status',
                                    'Command',
                                    'Slots_required',
                                    'Enqueue_time',
                                    'Start_time',
                                    'End_time',
                                    'Time_run',
                                    'Time_running']
                            )

    def test_get_state(self):
        """Test TaskSpooler.get_job_state()"""
        taskspooler = TaskSpooler()
        command = list()
        command.append(unicode("sleep", 'utf-8'))
        command.append(unicode("3", 'utf-8'))

        job_id = taskspooler.add_job(command=command)

        self.assertEqual(type(str()), type(taskspooler.get_job_state(job_id)))
        try:
            self.assertEqual(taskspooler.get_job_state(job_id), 'running')
            time.sleep(3)
            self.assertEqual(taskspooler.get_job_state(job_id), 'finished')
        except AssertionError:
            self.assertEqual(taskspooler.get_job_state(job_id), 'queued')
