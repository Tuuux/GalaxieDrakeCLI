#!/usr/bin/env python
# -*- coding: utf-8 -*-

import unittest
from GalaxieDrake.utils import Utils
import os
import re


# Unittest
class TestUtils(unittest.TestCase):
    # Test
    def test_get_title_from_video_file(self):
        """Test Utils.get_title_from_video_file()"""
        utils = Utils()
        self.assertEqual('Galaxie', utils.get_title_from_video_file('/home/lulu/Galaxie - 1080p.mkv'))

    def test_which(self):
        """Test Utils.which()"""
        utils = Utils()
        # return must be a str type or None
        self.assertEquals(type(utils.which('ls')), type(str()))
        self.assertEquals(utils.which('Galaxie'), None)
        # test to search in path
        self.assertEquals(utils.which('ls'), '/bin/ls')
        # test with the full path
        self.assertEquals(utils.which('/bin/ls'), '/bin/ls')
        # test raise error
        self.assertRaises(TypeError, utils.which, None)

    def test_get_mkvmerge_path(self):
        """Test Utils.get_mkvmerge_path()"""
        utils = Utils()
        # the return must be the same as which
        self.assertEquals(utils.which('mkvmerge'), utils.get_mkvmerge_path())
        # the dd path return must exist
        self.assertTrue(os.path.isfile(utils.get_mkvmerge_path()))
        # return None if nice is not found
        self.assertRaises(utils.get_mkvmerge_path(mkvmerge_app_name='Galaxie'), None)

    def test_get_dd_path(self):
        """Test Utils.get_dd_path()"""
        utils = Utils()
        # the return must be the same as which
        self.assertEquals(utils.which('dd'), utils.get_dd_path())
        # the dd path return must exist
        self.assertTrue(os.path.isfile(utils.get_dd_path()))
        # return None if nice is not found
        self.assertRaises(utils.get_dd_path(dd_app_name='Galaxie'), None)

    def test_get_dd_version(self):
        """Test Utils.get_dd_version()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_dd_version()))
        # must look like 'dd v8.26'
        self.assertTrue(re.search('^[0-9a-zA-Z_]+\sv[0-9.]+$', utils.get_dd_version()))

    def test_get_mkvextract_path(self):
        """Test Utils.get_mkvextract_path()"""
        utils = Utils()
        # the return must be the same as which
        self.assertEquals(utils.which('mkvextract'), utils.get_mkvextract_path())
        # the mkvextract path return must exist
        self.assertTrue(os.path.isfile(utils.get_mkvextract_path()))
        # return None if nice is not found
        self.assertRaises(utils.get_mkvextract_path(mkvextract_app_name='Galaxie'), None)

    def test_get_mkvextract_version(self):
        """Test Utils.get_mkvextract_version()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_mkvextract_version()))
        # must look like 'mkvextract v9.8.0'
        self.assertTrue(re.search('^[0-9a-zA-Z_]+\sv[0-9.]+$', utils.get_mkvextract_version()))

    def test_get_mediainfo_path(self):
        """Test Utils.get_mediainfo_path()"""
        utils = Utils()
        # the return must be the same as which
        self.assertEquals(utils.which('mediainfo'), utils.get_mediainfo_path())
        # the mediainfo path return must exist
        self.assertTrue(os.path.isfile(utils.get_mediainfo_path()))
        # return None if nice is not found
        self.assertRaises(utils.get_mediainfo_path(mediainfo_app_name='Galaxie'), None)

    def test_get_mediainfo_version(self):
        """Test Utils.get_mediainfo_version()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_mediainfo_version()))
        # must look like 'MediaInfoLib v0.7.91'
        # Care about mediainfo haven't stable response like other Unix application
        # Mediainfo team have code a randomize function with they --version argument.
        # thanks a lot to the Mediainfo team about it joke. Really bad job.
        # i know code a --version is very hard, a simple sprint is to mush for you , i understand...
        self.assertTrue(re.search('^[0-9a-zA-Z_]+\sv[0-9.?]+$', utils.get_mediainfo_version()))

    def test_get_nice_path(self):
        """Test Utils.get_nice_path()"""
        utils = Utils()
        # the return must be the same as which
        self.assertEquals(utils.which('nice'), utils.get_nice_path())
        # the nice path return must exist
        self.assertTrue(os.path.isfile(utils.get_nice_path()))
        # return None if nice is not found
        self.assertRaises(utils.get_nice_path(nice_app_name='Galaxie'), None)

    def test_get_nice_version(self):
        """Test Utils.get_nice_version()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_nice_version()))
        # must look like 'MediaInfoLib v0.7.91'
        self.assertTrue(re.search('^[0-9a-zA-Z_]+\sv[0-9.]+$', utils.get_nice_version()))

    def test_get_subripper_filename(self):
        """Test Utils.get_subripper_filename()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_subripper_filename()))
        # must return 'transcoder.py'
        self.assertEquals('subripper.py', utils.get_subripper_filename())

    def test_get_subripper_path(self):
        """Test Utils.get_subripper_path()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_subripper_path()))
        # the transcoder must exist
        self.assertTrue(os.path.isfile(utils.get_subripper_path()))

    def test_get_transcoder_filename(self):
        """Test Utils.get_transcoder_filename()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_transcoder_filename()))
        # must return 'transcoder.py'
        self.assertEquals('transcoder.py', utils.get_transcoder_filename())

    def test_get_transcoder_path(self):
        """Test Utils.get_transcoder_path()"""
        utils = Utils()
        # must be a str
        self.assertEquals(type(str()), type(utils.get_transcoder_path()))
        # the transcoder must exist
        self.assertTrue(os.path.isfile(utils.get_transcoder_path()))

    def test_set_get_nice_priority(self):
        """Test Utils.set_nice_priority() and Utils.get_nice_priority()"""
        utils = Utils()
        value = 42
        # test default value must be 15
        # return must be int type
        self.assertEqual(type(utils.get_nice_priority()), type(int()))
        # default value is 15
        self.assertEqual(utils.get_nice_priority(), 15)
        # set value and compare result
        utils.set_nice_priority(value)
        self.assertEqual(utils.get_nice_priority(), value)
        # test raise errors
        self.assertRaises(TypeError, utils.set_nice_priority, float())
