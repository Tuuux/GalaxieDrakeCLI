#!/usr/bin/env python
# -*- coding: utf-8 -*-

# It script it publish under GNU GENERAL PUBLIC LICENSE
# http://www.gnu.org/licenses/gpl-3.0.en.html
# Author: "Tuux" <tuxa@rtnp.org> all rights reserv

import unittest
import sys
import os
import re
import contextlib

from GalaxieDrake.subripper import SubRipper
from GalaxieDrake.utils import Utils


@contextlib.contextmanager
def capture():
    from cStringIO import StringIO
    oldout, olderr = sys.stdout, sys.stderr
    try:
        out=[StringIO(), StringIO()]
        sys.stdout,sys.stderr = out
        yield out
    finally:
        sys.stdout,sys.stderr = oldout, olderr
        out[0] = out[0].getvalue()
        out[1] = out[1].getvalue()


# Unittest
class TestSubRipper(unittest.TestCase):

    def test_get_desired_subtitle_filename(self):
        """Test SubRipper.get_desired_subtitle_filename()"""
        subripper = SubRipper()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        title = 'hello Galaxie'
        returned_value = subripper.get_desired_subtitle_filename(title=title, subtitle_track_info=track_info)
        self.assertEqual(title + '.' + track_info['Language'] + '.ass', returned_value)
        # None return
        returned_value = subripper.get_desired_subtitle_filename(title=title, subtitle_track_info=dict())
        self.assertEqual(None, returned_value)
        returned_value = subripper.get_desired_subtitle_filename(title=title, subtitle_track_info=None)
        self.assertEqual(None, returned_value)
        # error raises
        self.assertRaises(SystemError, subripper.get_desired_subtitle_filename)
        self.assertRaises(TypeError, subripper.get_desired_subtitle_filename, title=int())

    def test_get_actual_subtitle_filename(self):
        """Test SubRipper.get_desired_subtitle_filename()"""
        subripper = SubRipper()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        title = 'hello Galaxie'
        returned_value = subripper.get_actual_subtitle_filename(title=title, subtitle_track_info=track_info)
        self.assertEqual(title + '.' + track_info['Language'] + '_' + track_info['RandomID'] + '.ass', returned_value)
        # None return
        returned_value = subripper.get_actual_subtitle_filename(title=title, subtitle_track_info=dict())
        self.assertEqual(None, returned_value)
        returned_value = subripper.get_actual_subtitle_filename(title=title, subtitle_track_info=None)
        self.assertEqual(None, returned_value)
        # error raises
        self.assertRaises(SystemError, subripper.get_actual_subtitle_filename)
        self.assertRaises(TypeError, subripper.get_actual_subtitle_filename, title=int())

    def test_get_mkvextract_mode_track_line(self):
        """Test subripper.get_mkvextract_mode_track_line()"""
        subripper = SubRipper()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        # file and working directory are not set then return None
        self.assertRaises(SystemError, subripper.get_mkvextract_mode_track_line, subtitle_track_info=track_info)

        # set really something
        tested_directory = '/tmp'
        tested_filename = __file__
        subripper.set_mkv_file_path(tested_filename)
        subripper.set_working_directory(tested_directory)

        # without dictionary as argument it continue to return None
        returned_value = subripper.get_mkvextract_mode_track_line(subtitle_track_info=None)

        self.assertEqual(returned_value, None)

        returned_value = subripper.get_mkvextract_mode_track_line(subtitle_track_info=track_info)
        self.assertTrue('0:/tmp/test_subripper.French_FAFAFAFA.ass' in returned_value)
        # errors raise
        subripper = SubRipper()
        self.assertRaises(SystemError, subripper.get_cmd_for_extract_subtitle)
        self.assertRaises(TypeError, subripper.get_cmd_for_extract_subtitle, file_source=float(42))

    def test_rename_subtitles_files(self):
        """Test subripper.rename_subtitles_files()"""
        subripper = SubRipper()

        subripper.set_scan(subtitles_list=None)

        with capture() as output:
            subripper.rename_subtitles_files()

        # function should have no return in case it where scan list is empty
        self.assertEqual('', output[0])

        subripper.set_scan(subtitles_list=None)
        scan_list = list()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        scan_list.append(track_info)

        subripper.set_scan(subtitles_list=scan_list)

        with capture() as output:
            subripper.rename_subtitles_files()

        # # generate a list with the return
        # return_lines = output[0].strip().split('\n')
        #
        # # if no subtitles is in scan list, no file have to be rename
        # self.assertEqual(1, len(return_lines))
        #
        # self.assertTrue(re.search('^[0-9a-zA-Z_\s]+:$', return_lines[0]))
        # self.assertEqual('coucou', output)
        # self.assertEqual(3, len(return_lines))

    def test_get_cmd_for_extract_subtitle(self):
        """Test SubRipper.get_cmd_for_extract_subtitle()"""
        subripper = SubRipper()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        # prepare value's to check
        self.assertEqual(type(list()), type(subripper.get_cmd_for_extract_subtitle(
            file_source=__file__,
            subtitle_track_info=track_info
        )))
        # set really something
        tested_directory = '/tmp'
        tested_filename = __file__
        subripper.set_mkv_file_path(tested_filename)
        subripper.set_working_directory(tested_directory)

        wanted_value = list()
        wanted_value.append(unicode(subripper.utils.get_mkvextract_path(), 'utf-8'))
        wanted_value.append(unicode('tracks', 'utf-8'))
        wanted_value.append(unicode(tested_filename, 'utf-8'))
        wanted_value.append(unicode('0:/tmp/test_subripper.French_FAFAFAFA.ass', 'utf-8'))
        returned_value = subripper.get_cmd_for_extract_subtitle(
            file_source=tested_filename,
            subtitle_track_info=track_info
        )
        # compare
        self.assertEqual(wanted_value, returned_value)
        # errors raise
        subripper = SubRipper()
        self.assertRaises(SystemError, subripper.get_cmd_for_extract_subtitle)
        self.assertRaises(TypeError, subripper.get_cmd_for_extract_subtitle, file_source=float(42))
        self.assertRaises(
            TypeError,
            subripper.get_cmd_for_extract_subtitle,
            file_source=__file__,
            subtitle_track_info=float(42)
        )
        self.assertRaises(
            IOError,
            subripper.get_cmd_for_extract_subtitle,
            file_source=__file__ + 'lulu',
            subtitle_track_info=track_info
        )

    def test_get_cmd_for_generate_a_mks_file(self):
        """Test SubRipper.get_cmd_for_generate_a_mks_file()"""
        subripper = SubRipper()
        # prepare value's to check
        wanted_value = list()
        wanted_value.append(unicode(subripper.utils.get_mkvmerge_path(), 'utf-8'))
        wanted_value.append(unicode('--output', 'utf-8'))
        wanted_value.append(unicode(os.path.abspath(__file__ + '.mks'), 'utf-8'))
        wanted_value.append(unicode('--no-audio', 'utf-8'))
        wanted_value.append(unicode('--no-video', 'utf-8'))
        wanted_value.append(unicode('--no-buttons', 'utf-8'))
        wanted_value.append(unicode('--no-attachments', 'utf-8'))
        wanted_value.append(unicode('--no-chapters', 'utf-8'))
        wanted_value.append(unicode(os.path.abspath(__file__), 'utf-8'))

        returned_value = subripper.get_cmd_for_generate_a_mks_file(
            file_source=os.path.abspath(__file__),
            file_destination=os.path.abspath(__file__ + '.mks')

        )
        # compare again
        self.assertEqual(wanted_value, returned_value)
        # test raises errors
        self.assertRaises(
            TypeError,
            subripper.get_cmd_for_generate_a_mks_file,
            file_source=os.path.abspath(__file__),
            file_destination=None
        )
        self.assertRaises(
            TypeError,
            subripper.get_cmd_for_generate_a_mks_file,
            file_source=None,
            file_destination=os.path.abspath(__file__ + '.mks')
        )
        self.assertRaises(
            IOError,
            subripper.get_cmd_for_generate_a_mks_file,
            file_source=os.path.abspath(__file__ + 'la_la_la'),
            file_destination=os.path.abspath(__file__ + '.mks')
        )
        # don't understand 'lulu'
        # self.assertRaises(
        #     IOError,
        #     subripper.get_cmd_for_generate_a_mks_file,
        #     file_source=os.path.abspath(__file__),
        #     file_destination=os.path.abspath(os.path.sep + 'lulu' + '.mks')
        # )

    def test_get_cmd_for_scan_subtitles(self):
        """Test subripper.get_cmd_for_scan_subtitles()"""
        subripper = SubRipper()
        # prepare value's to check
        wanted_value = list()
        wanted_value.append(unicode(subripper.utils.get_mediainfo_path(), 'utf-8'))
        wanted_value.append(unicode(subripper.scan_subtitles_detect_line, 'utf-8'))
        wanted_value.append(unicode(os.path.abspath(__file__), 'utf-8'))
        returned_value = subripper.get_cmd_for_scan_subtitles(file_source=os.path.abspath(__file__))
        # compare
        self.assertEqual(wanted_value, returned_value)
        # a small retry by inject mediainfo
        returned_value = subripper.get_cmd_for_scan_subtitles(
            file_source=os.path.abspath(__file__),
            mediainfo_path='mediainfo'
        )
        # compare again
        self.assertEqual(wanted_value, returned_value)
        # raise error
        self.assertRaises(SystemError, subripper.get_cmd_for_scan_subtitles, file_source=None)
        self.assertRaises(
            SystemError,
            subripper.get_cmd_for_scan_subtitles,
            file_source=__file__,
            mediainfo_path='Galaxie'
        )

    def test_get_subtitle_extension_file(self):
        """Test subripper.get_subtitle_extension_file()"""
        subripper = SubRipper()
        self.assertEqual('.srt', subripper.get_subtitle_extension_file(codec=None))
        self.assertEqual('.srt', subripper.get_subtitle_extension_file(codec='42'))
        self.assertEqual('.srt', subripper.get_subtitle_extension_file(codec='S_TEXT/UTF8'))
        self.assertEqual('.ssa', subripper.get_subtitle_extension_file(codec='S_TEXT/SSA'))
        self.assertEqual('.ass', subripper.get_subtitle_extension_file(codec='S_TEXT/ASS'))
        self.assertEqual('.ogg', subripper.get_subtitle_extension_file(codec='S_KATE'))
        self.assertEqual('.sub', subripper.get_subtitle_extension_file(codec='S_VOBSUB'))
        self.assertEqual('.usf', subripper.get_subtitle_extension_file(codec='S_TEXT/USF'))
        self.assertEqual('.sup', subripper.get_subtitle_extension_file(codec='S_HDMV/PGS'))

    def test_set_get_scan(self):
        """Test subripper.set_scan() and subripper.get_scan()"""
        subripper = SubRipper()
        list_to_test = list()
        list_to_test.append(42)
        subripper.set_scan(subtitles_list=list_to_test)
        self.assertEqual(subripper.get_scan(), list_to_test)

    def test_get_mks_file_path(self):
        """Test subripper.get_mks_filename()"""
        subripper = SubRipper()
        # set everything to None
        subripper.set_mkv_file_path(filename=None)
        subripper.set_working_directory(directory=None)
        self.assertEqual(None, subripper.get_mks_file_path())
        # if file is not set it continue to return None
        subripper.set_working_directory(directory='.')
        self.assertEqual(None, subripper.get_mks_file_path())
        # set really something
        tested_directory = '/tmp'
        tested_filename = __file__
        subripper.set_mkv_file_path(tested_filename)
        subripper.set_working_directory(tested_directory)

        wanted_value = tested_directory
        wanted_value += os.path.sep
        wanted_value += os.path.basename(os.path.splitext(tested_filename)[0])
        wanted_value += '.mks'
        self.assertEqual(wanted_value, subripper.get_mks_file_path())

    def test_set_get_mkv_file_path(self):
        """Test subripper.set_file() and subripper.get_file()"""
        subripper = SubRipper()
        # the default value should be None
        self.assertEqual(subripper.get_mkv_file_path(), None)
        self.assertEqual(subripper.get_working_directory(), None)
        subripper.set_mkv_file_path(
            os.path.join(
                os.getcwd(),
                'test',
                'test_subripper.py'
            )
        )
        # workaround the joke of __file__ where sometime it return  .py and sometime .pyc
        # seriously , __file is not stable value, and be random .py or .pyc extension file
        try:
            self.assertEqual(subripper.get_mkv_file_path(), os.path.abspath(__file__))
        except AssertionError:
            self.assertEqual(subripper.get_mkv_file_path() + 'c', os.path.abspath(__file__))

        # set file must update the working directory
        self.assertEqual(subripper.get_working_directory(), os.path.dirname(os.path.abspath(__file__)))
        # set file to none
        subripper.set_mkv_file_path(None)
        self.assertEqual(subripper.get_mkv_file_path(), None)
        self.assertEqual(subripper.get_working_directory(), None)
        # raise error
        self.assertRaises(SystemError, subripper.set_mkv_file_path, 'Galaxie.mkv')
        # test the set via the init
        subripper = SubRipper(filename=os.path.abspath(__file__))
        self.assertEqual(subripper.get_mkv_file_path(), os.path.abspath(__file__))

    def test_set_get_working_directory(self):
        """Test subripper.set_working_directory() and subripper.get_working_directory()"""
        subripper = SubRipper()
        # the default value should be None
        self.assertEqual(subripper.get_working_directory(), None)
        # set _ get
        subripper.set_working_directory('.' + os.path.sep)
        self.assertEqual(subripper.get_working_directory(), os.getcwd())
        subripper.set_working_directory('.' + os.path.sep + '..')
        self.assertEqual(subripper.get_working_directory(), os.path.abspath(os.getcwd() + os.path.sep + '..'))
        # Back to none
        subripper.set_working_directory(None)
        self.assertEqual(subripper.get_working_directory(), None)
        # permit to set a other working directory
        subripper = SubRipper()
        subripper.set_mkv_file_path(os.path.abspath(__file__))
        self.assertEqual(subripper.get_working_directory(), os.path.dirname(os.path.abspath(__file__)))
        subripper.set_working_directory(os.path.abspath(os.getcwd() + os.path.sep + '..'))
        self.assertEqual(subripper.get_working_directory(), os.path.abspath(os.getcwd() + os.path.sep + '..'))
        # workaround the joke of __file__ where sometime it .py and sometime .pyc
        # seriously , __file is not stable value, and be random .py or .pyc extension file
        try:
            self.assertEqual(subripper.get_mkv_file_path(), os.path.abspath(__file__))
        except AssertionError:
            self.assertEqual(subripper.get_mkv_file_path() + 'c', os.path.abspath(__file__))

        # if it have a file set, the working directory can't be None
        subripper.set_mkv_file_path(os.path.abspath(__file__))
        subripper.set_working_directory(None)
        self.assertEqual(subripper.get_working_directory(), os.path.dirname(os.path.abspath(__file__)))

    def test_set_get_name(self):
        """Test subripper.set_name() and subripper.get_name()"""
        subripper = SubRipper()
        # the default value should be the class name
        self.assertEqual(subripper.__class__.__name__, subripper.get_name())
        # check set / get
        subripper.set_name('Galaxie')
        self.assertEqual(subripper.get_name(), 'Galaxie')
        # Raise error where name is not a str type
        self.assertRaises(TypeError, subripper.set_name, int(42))
        # try to set via init
        subripper = SubRipper(app_name='Galaxie')
        self.assertEqual(subripper.get_name(), 'Galaxie')

    def test_set_get_version(self):
        """Test subripper.set_version() and subripper.get_version()"""
        subripper = SubRipper()
        # the default value should be 0.4
        self.assertEqual(0.5, subripper.get_version())
        # check set / get
        subripper.set_version(4.2)
        self.assertEqual(4.2, subripper.get_version())
        # Raise a error where it's not a float type
        self.assertRaises(TypeError, subripper.set_version, 'Galaxie')
        # try to set via the init
        subripper = SubRipper(app_version=4.2)
        self.assertEqual(subripper.get_version(), 4.2)

    def test__display_header(self):
        """Test subripper._display_header()"""
        subripper = SubRipper()
        with capture() as output:
            subripper._display_header()
        match_line = '^' \
                     '[0-9a-zA-Z_]+\sv[0-9.?]+' \
                     ',\s' \
                     '[0-9a-zA-Z_]+\sv[0-9.?]+' \
                     ',\s' \
                     '[0-9a-zA-Z_]+\sv[0-9.?]+' \
                     '$'
        self.assertTrue(re.search(match_line, output[0].strip()))

    def test__display_summary_generate_mks_file(self):
        """Test subripper._di_display_summary_generate_mks_filesplay_summary_before_scan()"""
        # create a Subripper
        subripper = SubRipper()
        with capture() as output:
            subripper._display_summary_generate_mks_file()
        # create our search pattern
        # match_line = '^[0-9a-zA-Z_\s:]+\\n[0-9a-zA-Z_\s:]+None\\n[0-9a-zA-Z_\s:]+None\\n[0-9a-zA-Z_\s:]+None$'
        match_line = '^[0-9a-zA-Z_\s:]+'
        match_line += '\\n'
        match_line += '[0-9a-zA-Z_\s:]+'
        match_line += 'None'
        match_line += '[0-9a-zA-Z_\s:]+'
        match_line += 'None'
        match_line += '\\n'
        match_line += '[0-9a-zA-Z_\s:]+'
        match_line += 'None'
        match_line += '$'

        self.assertTrue(re.search(match_line, output[0].strip()))

        # set file and working directory
        subripper.set_mkv_file_path(os.path.abspath(__file__))
        subripper.set_working_directory(os.path.dirname(os.path.abspath(__file__)))
        # capture the output
        with capture() as output:
            subripper._display_summary_generate_mks_file()
        # create our new search pattern
        match_line = '^[0-9a-zA-Z_\s:]+'
        match_line += '\\n'
        match_line += '[0-9a-zA-Z_\s:]+'
        match_line += os.path.dirname(os.path.abspath(__file__))
        match_line += '/\\n'
        match_line += '[0-9a-zA-Z_\s:]+'
        match_line += os.path.abspath(__file__)
        match_line += '\\n'
        match_line += '[0-9a-zA-Z_\s:]+'
        match_line += os.path.dirname(os.path.abspath(__file__))
        match_line += os.path.sep
        match_line += os.path.basename(os.path.splitext(__file__)[0])
        match_line += '.mks'
        match_line += '$'

        self.assertTrue(re.search(match_line, output[0].strip()))

    def test__display_summary_after_scan(self):
        """Test subripper._display_summary_after_scan()"""
        # create a Subripper
        subripper = SubRipper()
        with capture() as output:
            subripper._display_summary_after_scan()
        # the scan list is empty
        subripper.set_scan(subtitles_list=None)
        # result must be ''
        self.assertEqual('', output[0].strip())

        # check with 1 subtitles tracks
        # the scan list is empty
        subripper.set_scan(subtitles_list=None)
        scan_list = list()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        scan_list.append(track_info)

        subripper.set_scan(subtitles_list=scan_list)
        with capture() as output:
            subripper._display_summary_after_scan()

        # generate a list with the return
        return_lines = output[0].strip().split('\n')
        # it should return 2 lines , one for titles header and one by subtitles
        self.assertEqual(2, len(return_lines))
        # search in titles header th number of subtitles found
        captured_text = re.findall('^([0-9]+)', return_lines[0])
        self.assertEqual('1', captured_text[0])

        # check with 2 subtitles tracks
        # the scan list is empty
        subripper.set_scan(subtitles_list=None)
        scan_list = list()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        scan_list.append(track_info)
        track_info = {
            'ID': '1',
            'Language': 'French',
            'Default': 'No',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FBFBFBFB'
        }
        scan_list.append(track_info)
        subripper.set_scan(subtitles_list=scan_list)
        with capture() as output:
            subripper._display_summary_after_scan()

        # generate a list with the return
        return_lines = output[0].strip().split('\n')
        # it should return 3 lines , one for titles header and one by subtitles
        self.assertEqual(3, len(return_lines))
        # search in titles header th number of subtitles found
        captured_text = re.findall('^([0-9]+)', return_lines[0])
        self.assertEqual('2', captured_text[0])

        # check if second line have good format with 7 elements
        self.assertEqual(7, len(return_lines[1].split(',')))

    def test__display_subtitles_extraction_header(self):
        """Test subripper._display_subtitles_extraction_header()"""
        # create a Subripper
        subripper = SubRipper()
        # the scan list is empty
        subripper.set_scan(subtitles_list=None)
        with capture() as output:
            subripper._display_subtitles_extraction_header()
        # result must be ''
        self.assertEqual('', output[0].strip())

        # test with scan not empty
        fake_list = list()
        fake_list.append(42)
        subripper.set_scan(subtitles_list=fake_list)
        with capture() as output:
            subripper._display_subtitles_extraction_header()
        # generate a list with the return
        return_lines = output[0].strip().split('\n')
        # it should return 1 line
        self.assertEqual(1, len(return_lines))
        # search in titles header the number of subtitles found
        captured_text = re.findall('^[0-9a-zA-Z_\s]+([0-9]+)[0-9a-zA-Z_\s]+:$', return_lines[0])
        self.assertEqual('1', captured_text[0])

        # try with 2
        fake_list.append(42)
        subripper.set_scan(subtitles_list=fake_list)
        with capture() as output:
            subripper._display_subtitles_extraction_header()
        # generate a list with the return
        return_lines = output[0].strip().split('\n')
        # it should return 1 line
        self.assertEqual(1, len(return_lines))
        # search in titles header the number of subtitles found
        captured_text = re.findall('^[0-9a-zA-Z_\s]+([0-9]+)[0-9a-zA-Z_\s]+:$', return_lines[0])
        self.assertEqual('2', captured_text[0])

    def test__display_subtitles_extraction_summary_by_track(self):
        """Test subripper._display_subtitles_extraction_summary_by_track()"""
        # create a Subripper
        subripper = SubRipper()
        # the scan list is empty
        subripper.set_scan(subtitles_list=None)
        with capture() as output:
            subripper._display_subtitles_extraction_summary_by_track()
        # result must be ''
        self.assertEqual('', output[0].strip())

        scan_list = list()
        track_info = {
            'ID': '0',
            'Language': 'French',
            'Default': 'Yes',
            'Forced': 'No',
            'Format': 'ASS',
            'CodecID': 'S_TEXT/ASS',
            'CodecInfo': 'Advanced Sub Station Alpha',
            'RandomID': 'FAFAFAFA'
        }
        scan_list.append(track_info)
        subripper.set_scan(subtitles_list=scan_list)
        subripper.set_mkv_file_path(__file__)
        subripper.set_working_directory(directory='/tmp')

        with capture() as output:
            subripper._display_subtitles_extraction_summary_by_track(track_info)
        # generate a list with the return
        return_lines = output[0].strip().split('\n')
        # it should return a list 8 elements
        self.assertEqual(8, len(return_lines))
        self.assertEqual(type(return_lines), type(list()))
        # 'Track: 0'
        self.assertTrue('Track: 0' in return_lines)
        # ' Language: French'
        self.assertTrue(' Language: French' in return_lines)
        # ' Format: ASS'
        self.assertTrue(' Format: ASS' in return_lines)
        # ' Codec: S_TEXT/ASS'
        self.assertTrue(' Codec: S_TEXT/ASS' in return_lines)
        # ' Codec Info: Advanced Sub Station Alpha'
        self.assertTrue(' Codec Info: Advanced Sub Station Alpha' in return_lines)
        # ' Filename: test_subripper.French_FAFAFAFA.ass'
        # ' Working Directory: /tmp/'
        self.assertTrue(' Working Directory: /tmp/' in return_lines)
        # ' Random ID: FAFAFAFA'
        self.assertTrue(' Random ID: FAFAFAFA' in return_lines)

    def test__display_nothing(self):
        """Test subripper._display_nothing_do_do()"""
        subripper = SubRipper()
        with capture() as output:
            subripper._display_nothing_do_do()
        match_line = '^[0-9a-zA-Z_\s]+\s+...$'
        self.assertTrue(re.search(match_line, output[0].strip()))
