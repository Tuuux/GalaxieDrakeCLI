==========================
GalaxieDrake Documentation
==========================
.. figure::  images/logo_galaxie.png
   :align:   center

Once upon a time, this project was hosted on a
ancient platform called GitHub. Then came the Buyer.
The Buyer bought GitHub, willing to rule over its community.
**I was not to sell**, so here is the new home of "https://github.com/Tuuux/GalaxieDrakeCLI".

The Project
-----------
**GalaxieDrake** is semi automatized Text Based Media Center ToolKit under GPL v3 free software license.
It use different tool's in background like mediainfo, mkvmerge; mkvextract, handbrake-cli and is write with Python.

Originally the project have start in 2016 when the author Jérôme.O have start to learn Python.

GalaxieDrake provide Command Line Interface tool and a Python API. if you searching a peace of code you certainly
found it , and don't hesitate to copy it .

The Mission
-----------
Provide a Text Based tool for create/maintain a standardized MediaCenter.
Unfortunately all commercial solution, want to jail you personal data's inside cloud or non open or standard format.
years after years the mess continue... The humanity need better ... we need free solution ...

Originally the author have create few script's to help him to transcode video's take with him PVR. (Personal Video Recorder)
The original source was EU MPEG .ts format, in destination to Matroska container.
Later the need of transcoding video from many source like camera, smartphone to many destination like a home multi-display.

Actually the main format target is Matroska container, h.264 profile 4.1, FullHD 1920x1080 it be a good standard and
the one use by RasberryPI

All GalaxieDrake tool's, try to make automation possible, and back to a Open Containt standard format.

The final goal is to provide a Pure Curses Text based and Command line Transcoding tool suite.
Why to it ? that because they tool are suppose to run inside a NAS or a Minimal GNU/Linux system over SSH.

Don't see the GalaxieDrake suite tool as a pirate usage.

During lot of years the main stream was to provide big computer with big GUI Toolkit,
unfortunately almost nobody have care about ultra low profile computer and we are now in a situation where no mature
ToolKit is ready to use on **pen computer**. Time's change then it's time to change the world ...

Help
----
prepare.py
----------

Usage:
  prepare.py [options] directory

  Please, invoke it script with the path of a directory
  It directory will be use as Movie source directory

Few Examples:
  .. code-block:: bash

     prepare.py ./
     prepare.py ../a/other/directory
     prepare.py /full/path/to/the/directory
     prepare.py "/full/path/to/the/directory/With space name/"

Positional arguments:
  directory             Working directory where start the media scan

Optional arguments:
  -h, --help

    show this help message and exit

  -y, --yes, --assume-yes

    Automatic yes to prompts; assume "yes" as answer to all prompts and run non-interactively.

  -n, --no, --assume-no

    Automatic "no" to all prompts; assume "no" as answer to all prompts and run non-interactively.

  -P, --progress

    This option tells prepare.py to print information showing the progress of the scan. This gives a bored user something to watch.

  -v, --verbosity

    increase output verbosity, -v or -vv or -vvv are accepted

  -V, --version

    show program's version number and exit


Example
-------

.. code-block:: bash

    # ./prepare.py -P /home/
    Prepare v0.3 / SubRipper v0.5 / Task Spooler v1.0 / nice v8.26
    Transcoder Path: /home/user/PycharmProjects/GalaxieDrakeCLI/GalaxieDrakeCLI/transcoder.py
    Source Directory: /home/
    Searching for:
     TS MKV 3G2 3GP ASF ASX AVI FLV M4V MOV MP4 MPG RM SWF VOB WMV
     Exception Pattern: "* - *p.mkv"
    1 File found:
     /home/user/Videos/test/super_video.mkv

    Do you want creat transcoding task for it file ? [Y/n] n
    Nothing to do ...


Features
--------
* Scan recursively directory for discover video to transcode
* TaskSpooler , with persistent task
* Priority task
* Video transcoder
* Subtitles extractor
* Common thing for a text based tool's , like progress bar, and soon a Curses GUI :)

Contribute
----------

- Issue Tracker: https://gitlab.com/Tuuux/GalaxieDrakeCLI/issues
- Source Code: https://gitlab.com/Tuuux/GalaxieDrakeCLI

Our collaboration model is the Collective Code Construction Contract (C4): https://rfc.zeromq.org/spec:22/C4/

Documentations
--------------
.. toctree::
   :maxdepth: 2

   Install
   GalaxieDrake
   Basic-Types
   Contributors

License
-------
GNU GENERAL PUBLIC LICENSE Version 3


See the LICENCE_

.. _LICENCE: https://gitlab.com/Tuuux/GalaxieDrakeCLI/blob/master/LICENSE.txt

All contributions to the project source code ("patches") SHALL use the same license as the project.

Indices and tables
------------------
* :ref:`genindex`
* :ref:`search`