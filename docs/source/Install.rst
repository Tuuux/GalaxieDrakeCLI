.. _instalation:

============
Installation
============
You can found the GalaxieDrake Repository here:
https://github.com/Tuuux/GalaxieDrakeCLI

In any case it consist to copy the package GalaxieDrakeCLI inside you computer.

Yet it haven't any packages installer, like RPM or DEB

Before you start, make sure that you already have installed **Python**, **pip** and **git**.

* Then clone GalaxieDrakeCLI project from GitHub:

.. code:: bash

    git clone https://github.com/Tuuux/GalaxieDrakeCLI.git


It will create a folder name ``GalaxieDrakeCLI`` it contain the ``GalaxieDrakeCLI`` package:

* Add this second directory in you PATH environement variable .

* Each tool have a --help

.. code:: bash

   prepare ./


Next Step:
----------
* Have a API ready to be use with Galaxie Curses, for have a GUI to that tool suite
* Enjoy ;-)
